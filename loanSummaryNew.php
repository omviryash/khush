<?php
include "include/config.inc.php";
$loanArray      = array();
$loanArray1      = array();
$partyArr       = array();
$agentArr       = array();
$gaurantorArr   = array();
$curDate        = date('Y-m-d');
$curDate        = isset($_GET['curDateYear']) ? $_GET['curDateYear']."-".$_GET['curDateMonth']."-".$_GET['curDateDay'] : date("Y-m-d"); 
$selectedParty  = isset($_GET['partyId']) ? $_GET['partyId'] : 0;
$selectedAgent  = isset($_GET['agentId']) ? $_GET['agentId'] : 0;
$selectedGaurantor = isset($_GET['gaurantorId']) ? $_GET['gaurantorId'] : 0;
$i = 0;

// Party Select
$j = 0;

$selPartyQry = "SELECT partyId,partyName
                  FROM party
              ORDER BY partyName";
$selPartyQryRes = mysql_query($selPartyQry);
while($partyRow = mysql_fetch_array($selPartyQryRes))
{
	$partyArr['partyId'][$j]   = $partyRow['partyId'];
	$partyArr['partyName'][$j] = $partyRow['partyName'];
	$j++;
}


// Agent Select
$j = 0;

$selAgentQry = "SELECT agent_id,agent_name
                  FROM agent
              ORDER BY agent_name";
$selAgentQryRes = mysql_query($selAgentQry);
while($agentRow = mysql_fetch_array($selAgentQryRes))
{
	$agentArr['agentId'][$j]   = $agentRow['agent_id'];
	$agentArr['agentName'][$j] = $agentRow['agent_name'];
	$j++;
}

// Guarantor Select

$j = 0;
$t = -1;

$gaurantorArr['partyId'][$j]   = 0;
$gaurantorArr['partyName'][$j] = "All Guarantor";
$t = 0;
$j++;

$selGuaranterQry = "SELECT distinct guaranterId from loan";
$selGuaranterQryRes = mysql_query($selGuaranterQry);
while($guaranterRow = mysql_fetch_array($selGuaranterQryRes))
{
	$q = "select partyId,partyName from party where partyId = ".$guaranterRow['guaranterId'];
	$rs = mysql_query($q);
	while($row = mysql_fetch_array($rs))
	{
		$gaurantorArr['partyId'][$j]   = $row['partyId'];
		$gaurantorArr['partyName'][$j] = $row['partyName'];
		$t = $row['partyId'];
	}
	$j++;
}


// Loan Display

$selLoanQry = "SELECT loanId,loanDate,loanAmount,interest,installmentAmount,loanTypeId,partyId,installmentDays
		                 FROM loan where loanDate <= '".$curDate."'";

if($selectedParty > 0)
{
	$selLoanQry = "SELECT loanId,loanDate,loanAmount,interest,installmentAmount,loanTypeId,partyId,installmentDays
		                 FROM loan where partyId = ".$selectedParty." and loanDate <= '".$curDate."'";

}
if($selectedAgent > 0)
{
	$selLoanQry = "SELECT loanId,loanDate,loanAmount,interest,installmentAmount,loanTypeId,partyId,installmentDays
		                 FROM loan where agentId = ".$selectedAgent." and loanDate <= '".$curDate."'";

}
if($selectedGaurantor > 0)
{
	$selLoanQry = "SELECT loanId,loanDate,loanAmount,interest,installmentAmount,loanTypeId,partyId,installmentDays
		                 FROM loan where guaranterId = ".$selectedGaurantor." and loanDate <= '".$curDate."'";

}
if($selectedParty > 0 && $selectedAgent > 0)
{
	$selLoanQry = "SELECT loanId,loanDate,loanAmount,interest,installmentAmount,loanTypeId,partyId,installmentDays
		                 FROM loan where partyId = ".$selectedParty." and agentId = ".$selectedAgent." and loanDate <= '".$curDate."'";
}
if($selectedParty > 0 && $selectedGaurantor > 0)
{
	$selLoanQry = "SELECT loanId,loanDate,loanAmount,interest,installmentAmount,loanTypeId,partyId,installmentDays
		                 FROM loan where partyId = ".$selectedParty." and guaranterId = ".$selectedGaurantor." and loanDate <= '".$curDate."'";
}
if($selectedGaurantor > 0 && $selectedAgent > 0)
{
	$selLoanQry = "SELECT loanId,loanDate,loanAmount,interest,installmentAmount,loanTypeId,partyId,installmentDays
		                 FROM loan where guaranterId = ".$selectedGaurantor." and agentId = ".$selectedAgent." and loanDate <= '".$curDate."'";
}
if($selectedParty > 0 && $selectedAgent > 0 && $selectedGaurantor > 0)
{
	$selLoanQry = "SELECT loanId,loanDate,loanAmount,interest,installmentAmount,loanTypeId,partyId,installmentDays
		                 FROM loan where partyId = ".$selectedParty." and agentId = ".$selectedAgent." and guaranterId = ".$selectedGaurantor." and loanDate <= '".$curDate."'";
}

$selLoanQryRes = mysql_query($selLoanQry) or print mysql_error();

while($loanRow = mysql_fetch_array($selLoanQryRes))
{
	$loanArray[$i]['loanId']           = $loanRow['loanId'];
	$loanArray[$i]['partyId']        = $loanRow['partyId'];;
	
	
	
	$q = "select partyName from party where partyId = ".$loanRow['partyId'];
	$rs = mysql_query($q);
	
	$loanArray[$i]['partyName']        = mysql_result($rs,0,0);
	
	$q = "select loanType from loantype where loanTypeId = ".$loanRow['loanTypeId'];
	$rs = mysql_query($q);
	
	$loanArray[$i]['loanType']         = mysql_result($rs,0,0);
	
	
	$loanArray[$i]['loanDate']         = $loanRow['loanDate'];
	$loanArray[$i]['loanEndDate']	   = "";
	if($loanRow['loanTypeId'] == 1)
	{
		$date=date_create($loanRow['loanDate']);
		date_add($date,date_interval_create_from_date_string($loanRow['installmentDays']." days"));
		$loanArray[$i]['loanEndDate']	   = date_format($date,"Y-m-d");
	}
		
	$loanArray[$i]['loanAmount']       = $loanRow['loanAmount'];	
	
	$receivedAmount = 0;
	$interest = 0;
	if($loanRow['loanTypeId'] == 1)
	{
		$interest = $loanRow['interest'];	
	}
	$selReceivedAmount = "select * from transactionNew where loanId = ".$loanRow['loanId']." AND transactionDate <= '".$curDate."' ";
	
	$rsReceivedAmount = mysql_query($selReceivedAmount);
	while($rowReceivedAmount = mysql_fetch_array($rsReceivedAmount))
	{
		if($rowReceivedAmount['transactionForId'] == 1 && $rowReceivedAmount['creditDebit'] == 'Credit')
		{
			$receivedAmount = $receivedAmount + $rowReceivedAmount['transactionAmount'];
		}
		if($rowReceivedAmount['transactionForId'] == 2 && $rowReceivedAmount['creditDebit'] == 'Credit' && $loanRow['loanTypeId'] == 4)
		{
			$interest = $interest + $rowReceivedAmount['transactionAmount'];
		}
	}
	$loanArray[$i]['pendingInstallment'] = "";	
	if($loanRow['loanTypeId'] == 1)
	{	
		$selPendingInstallment = "select * from installment where loanId = ".$loanRow['loanId']." and installmentDate <= '".$curDate."' ";
		$selPendingInstallmentRes = mysql_query($selPendingInstallment);
		$pi = 0;
		while($selPendingInstallmentResRow = mysql_fetch_array($selPendingInstallmentRes))
		{
			if($selPendingInstallmentResRow['isReceived'] == 'N')
			{
				$pi = $pi + 1;
			}
		}
		$loanArray[$i]['pendingInstallment'] = $pi;
	}
	if($loanRow['loanTypeId'] == 4)
	{
		$selPendingInstallment 				= "select * from loan_details where loanId = ".$loanRow['loanId']." and due_date <= '".$curDate."' ";
		$selPendingInstallmentRes 			= mysql_query($selPendingInstallment);
		$pid = 0;
		if(mysql_num_rows($selPendingInstallmentRes) > 0 ) {
			while($selPendingInstallmentResRow = mysql_fetch_array($selPendingInstallmentRes))
			{
				if($selPendingInstallmentResRow['recevied'] == 0)
				{
					//$loanArray[$i]['pendingInstallment']  = "Pending";
					$pid = $pid + 1;
				}
			}
		}
		$loanArray[$i]['pendingInstallment'] = $pid;
	}
	$loanArray[$i]['interestReceived'] = $interest;
	$loanArray[$i]['receivedAmount']   = $receivedAmount;
	$loanArray[$i]['pendingAmount']    = $loanRow['loanAmount'] - $receivedAmount;	
	if($loanRow['loanTypeId'] == 4)
    $loanArray[$i]['pendingInterest']  = $loanArray[$i]['pendingAmount'] * $loanRow['interest'] * $loanArray[$i]['pendingInstallment'] / 100;
  else
    $loanArray[$i]['pendingInterest']  = 0;
	$i++;
}

if(!($selectedParty > 0) && !($selectedGaurantor > 0))
{
  $selLoanQry = "SELECT fd_id,fd_date,fd_amount,fd_interest_rate,fd_party_id
                  FROM fd_amount
                  WHERE fd_date <= '".$curDate."' ";
  if($selectedParty > 0)
  {
  	$selLoanQry = "SELECT fd_id,fd_date,fd_amount,fd_interest_rate,fd_party_id
                  FROM fd_amount
                  WHERE fd_party_id = ".$selectedParty." AND fd_date <= '".$curDate."' ";
  }
  $k = 0;
  $selLoanQryRes = mysql_query($selLoanQry);
  if(mysql_num_rows($selLoanQryRes) > 0 ) {
  	while($loanRow = mysql_fetch_array($selLoanQryRes))
  	{
  		//print_r($loanRow);exit;
  		$loanArray1[$k]['fd_id']           	= $loanRow['fd_id'];
  		$q = "select partyName from party where partyId = ".$loanRow['fd_party_id'];
  		$rs = mysql_query($q);
  	
  		$loanArray1[$k]['partyId']        = $loanRow['fd_party_id'];
  		$loanArray1[$k]['partyName']        = mysql_result($rs,0,0);
  	
  		$loanArray1[$k]['fd_interest_rate'] = $loanRow['fd_interest_rate'];
  		$loanArray1[$k]['fd_date']         	= $loanRow['fd_date'];
  		$loanArray1[$k]['loanType']         = 'FD';
  		$loanArray1[$k]['fd_amount']       	= $loanRow['fd_amount'];	
  		
  		$reciveLoanAmount 					= "select sum(transactionAmount) as transactionAmount from transactionnew where fd_id = ".$loanRow['fd_id']." AND creditDebit='Credit' AND transactionDate <= '".$curDate."' group by loanId";
  		$rsReciveLoanAmount 				= mysql_query($reciveLoanAmount);
  		
  		if(mysql_num_rows($rsReciveLoanAmount) > 0 ) {
  			$rsReciveLoanAmount 				 = mysql_fetch_array($rsReciveLoanAmount);
  			//print_r($rsReciveLoanAmount);
  			$loanArray1[$k]['pendingLoanAmount'] = $loanArray1[$k]['fd_amount'] - $rsReciveLoanAmount['transactionAmount'];
  		} else {
  			$loanArray1[$k]['pendingLoanAmount'] = $loanArray1[$k]['fd_amount'];
  		}
  		
  		$receivedAmountfd = 0;
  		$interestfd = 0;
  		$selReceivedAmount = "select * from transactionNew where fd_id = ".$loanRow['fd_id']." AND transactionDate <= '".$curDate."' ";
  		$rsReceivedAmount = mysql_query($selReceivedAmount);
  		while($rowReceivedAmount = mysql_fetch_array($rsReceivedAmount))
  		{
  			if($rowReceivedAmount['transactionForId'] == 5 && $rowReceivedAmount['creditDebit'] == 'Debit')
  			{
  				$receivedAmountfd = $receivedAmountfd + $rowReceivedAmount['transactionAmount'];
  			}
  			if($rowReceivedAmount['transactionForId'] == 6 && $rowReceivedAmount['creditDebit'] == 'Debit')
  			{
  				$interestfd = $interestfd + $rowReceivedAmount['transactionAmount'];
  			}
  		}
  		
  		
  		//$loanArray1[$k]['pendingInstallment']  = "Not Pending";
  		$selPendingInstallment 				= "select * from fd_details where fd_id = ".$loanRow['fd_id']." and due_date <= '".$curDate."' ";
  		$selPendingInstallmentRes 			= mysql_query($selPendingInstallment);
  		
  		$pifd = 0;
  		if(mysql_num_rows($selPendingInstallmentRes) > 0 ) {
  			while($selPendingInstallmentResRow = mysql_fetch_array($selPendingInstallmentRes))
  			{
  				if($selPendingInstallmentResRow['recevied'] == 0)
  				{
  					//$loanArray1[$k]['pendingInstallment']  = "Pending";
  					$pifd  = $pifd + 1;
  				}
  			}
  		}
  		$loanArray1[$k]['pendingInstallment'] = $pifd;
  		$loanArray1[$k]['interestReceived'] = $interestfd;
  		$loanArray1[$k]['receivedAmount']   = $receivedAmountfd;
  		$loanArray1[$k]['pendingAmount']    = $loanRow['fd_amount'] - $receivedAmountfd;	
  		$loanArray1[$k]['loanEndDate']	   = "";
  		$loanArray1[$k]['pendingInterest'] = (($loanArray1[$k]['fd_amount'] * $loanArray1[$k]['fd_interest_rate']) / 100) * $loanArray1[$k]['pendingInstallment'];
  		$k++;
  	}
  }
}

$smarty->assign("loanArray",$loanArray);
$smarty->assign("loanArray1",$loanArray1);
$smarty->assign("partyArr",$partyArr);
$smarty->assign("gaurantorArr",$gaurantorArr);
$smarty->assign("agentArr",$agentArr);
$smarty->assign("selectedParty",$selectedParty);
$smarty->assign("selectedAgent",$selectedAgent);
$smarty->assign("selectedGaurantor",$selectedGaurantor);
$smarty->assign("curDate",$curDate);
$smarty->assign("loanAmountTotal",0);
$smarty->assign("fdAmountTotal",0);
$smarty->assign("interestReceivedTotal",0);
$smarty->assign("interestPaidTotal",0);
$smarty->assign("receivedAmountTotal",0);
$smarty->assign("receivedAmountTotalFD",0);
$smarty->assign("pendingAmountTotal",0);
$smarty->assign("pendingAmountTotalFD",0);
$smarty->assign("pendingInterestTotal",0);
$smarty->assign("pendingInterestTotalFD",0);
$smarty->display('loanSummaryNew.tpl');
?>