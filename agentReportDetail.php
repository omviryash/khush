<?php
include "include/config.inc.php";
$agentId = 0;

if(isset($_GET['agentId']))
{
	$agentId = $_GET['agentId'];
}

$agent_list 			 = array();
$paidCommissionArr       = array();
$i = 0;
$j = 0;

$selectAgent = "select * from agent where agent_id = ".$agentId;
$rsAgent = mysql_query($selectAgent);
while($agentRow = mysql_fetch_array($rsAgent))
{
	$agentName = $agentRow['agent_name'];
	$selectLoan = "select * from loan where agentId = ".$agentRow['agent_id'];
	$rsLoan = mysql_query($selectLoan);
	while($loanRow = mysql_fetch_array($rsLoan))
	{
		$agent_list[$i]['loanId'] = $loanRow['loanId'];
		$q = "select partyName from party where partyId = ".$loanRow['partyId'];
		$rs = mysql_query($q);
		$agent_list[$i]['partyId'] = mysql_result($rs,0,0);
		$agent_list[$i]['loanDate'] = $loanRow['loanDate'];
		$agent_list[$i]['loanAmount'] = $loanRow['loanAmount'];
		$q = "select loanType from loantype where loanTypeId = ".$loanRow['loanTypeId'];
		$rs = mysql_query($q);
		$agent_list[$i]['loanTypeId'] = mysql_result($rs,0,0);
		$selectCommission = "select transactionAmount,creditDebit from transactionnew where loanId = ".$loanRow['loanId']." AND transactionForId = 2 AND agentId = ".$agentRow['agent_id']."";
		$rsCommission = mysql_query($selectCommission);
		$c = 0;
		while($commissionRow = mysql_fetch_array($rsCommission))
		{
			$a = $commissionRow['transactionAmount'];
			$r = $agentRow['commission'];
			if($commissionRow['creditDebit'] == 'Credit')
			{
				$c = $c + ($a * ($r/100));
			}
			else
			{
				$c = $c - ($a * ($r/100));
			}
		}
		$agent_list[$i]['commission'] = $c;
		$agent_list[$i]['commission_per'] = $agentRow['commission'];
		$i++;
	}
	$selectPaidCommission = "select loanId,transactionDate,note,transactionAmount from transactionnew where transactionForId = 4 AND creditDebit = 'Debit' AND agentId = ".$agentRow['agent_id']."";
	//echo $selectPaidCommission;
	//die;
	$rsPaidCommission = mysql_query($selectPaidCommission);
	while($paidCommissionRow = mysql_fetch_array($rsPaidCommission))
	{
		$paidCommissionArr[$j]['loanId'] = $paidCommissionRow['loanId'];
		$paidCommissionArr[$j]['transactionDate'] = $paidCommissionRow['transactionDate'];
		$paidCommissionArr[$j]['transactionAmount'] = $paidCommissionRow['transactionAmount'];
		$paidCommissionArr[$j]['note'] = $paidCommissionRow['note'];
		$j++;
	}
}

$smarty->assign("paidCommissionArr",$paidCommissionArr);
$smarty->assign("agentName",$agentName);
$smarty->assign("agent_list",$agent_list);
$smarty->display('agentReportDetail.tpl');
?>