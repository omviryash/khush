<?php
include "include/config.inc.php";
$loanDetailRow = array();
$i = 0;

$viewTypeSelected = isset($_REQUEST['viewType'])  ? $_REQUEST['viewType'] : "Pending"; 

$curDateYear  = isset($_REQUEST['curDateYear'])  ? $_REQUEST['curDateYear']  : date("Y"); 
$curDateMonth = isset($_REQUEST['curDateMonth']) ? $_REQUEST['curDateMonth'] : date("m"); 
$curDateDay   = isset($_REQUEST['curDateDay'])   ? $_REQUEST['curDateDay']   : date("d"); 
$curDate = isset($_REQUEST['curDateYear']) ? $_REQUEST['curDateYear']."-".$_REQUEST['curDateMonth']."-".$_REQUEST['curDateDay'] : date("Y-m-d"); 

if(isset($_REQUEST['installmentReceived']))
{
	foreach ($_REQUEST['installmentReceived'] as $key => $value)
	{
		if($value != "" && $value > 0)
		{
			echo "<BR> ".$key." : ".$value;
			for($i=1;$i<=$value;$i++)
			{
				$findInstallmenQuery = "SELECT MIN(installmentNo) AS minInstallmentNo
				                          FROM installment
				                         WHERE loanId = ".$key."
				                           AND isReceived = 'N'";
        $findInstallmenResult = mysql_query($findInstallmenQuery);
        if($findInstallmenRow = mysql_fetch_array($findInstallmenResult))
        {
				  $updateQuery = "UPDATE installment
				                     SET isReceived = 'Y', receiveDate = '".$curDate."'
				                   WHERE installmentNo = ".$findInstallmenRow['minInstallmentNo']."
				                     AND loanId = ".$key;
          $updateResult = mysql_query($updateQuery);
				  $updateQuery = "UPDATE loan
				                     SET installmentReceived = installmentReceived + 1
				                   WHERE loanId = ".$key;
          $updateResult = mysql_query($updateQuery);
        }
			}
		}
	}
	header("Location: loanDetail1.php");
	exit();
}

//$selParty = "SELECT partyId, partyName
//               FROM party";
///////////////////////
$selLoanDetail = "SELECT loan.loanId, party.partyName, loan.installmentReceived, loan.installmentAmount
                    FROM loan
                    JOIN party ON loan.partyId = party.partyId
                   WHERE installmentDays > installmentReceived
                    ";
$selLoanDetailRes = mysql_query($selLoanDetail);
while($selLoanDetailRow = mysql_fetch_array($selLoanDetailRes))
{
  $loanDetailRow[$i]['loanId']              = $selLoanDetailRow['loanId'];
  $loanDetailRow[$i]['partyName']           = $selLoanDetailRow['partyName'];
  $loanDetailRow[$i]['installmentReceived'] = $selLoanDetailRow['installmentReceived'];
  $loanDetailRow[$i]['installmentAmount']   = $selLoanDetailRow['installmentAmount'];
  $selLastInstallmentDate = "SELECT MAX(installment.installmentDate) AS lastInstallmentDate 
                               FROM installment 
                              WHERE installment.loanId =".$selLoanDetailRow['loanId']."
                                AND isReceived = 'Y'";
  $selLastInstallmentDateRes = mysql_query($selLastInstallmentDate);
  while($LastInstallmentDateRow = mysql_fetch_array($selLastInstallmentDateRes))
  {
    $loanDetailRow[$i]['lastInstallmentDate'] = $LastInstallmentDateRow['lastInstallmentDate'];
  }
  $i++;
}

$viewTypeValues[0] = "Pending";
$viewTypeOutput[0] = "Pending";
$viewTypeValues[1] = "All";
$viewTypeOutput[1] = "All";
$smarty->assign("viewTypeValues",$viewTypeValues);
$smarty->assign("viewTypeOutput",$viewTypeOutput);
$smarty->assign("viewTypeSelected",$viewTypeSelected);
$smarty->assign("curDate",$curDate);
$smarty->assign("loanDetailRow",$loanDetailRow);
$smarty->display('loanDetail1.tpl');
?>