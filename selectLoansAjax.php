<?php
  include "include/config.inc.php";
  $start=$_REQUEST['startDatePass'];
  $end=$_REQUEST['endDatePass'];
  $loanStatus = $_REQUEST['loanStatus'];
  $loans = array();
  $m= 0 ;
  if($loanStatus == "current")
  {
    $selectLoans = "SELECT loan.loanId, party.partyName as partyName, loan.loanDate, loan.loanAmount, loan.interest, loan.installmentAmount, loan.installmentDays, installment.isReceived
                      FROM loan
                      JOIN party ON party.PartyId = loan.partyId
                      JOIN installment ON loan.loanId = installment.loanId 
                     WHERE installment.isReceived = 'N' AND loan.loanDate  BETWEEN  '$start' AND  '$end'
                  GROUP BY loan.loanId ORDER BY loan.loanDate";
    $selectLoansRes = mysql_query($selectLoans);
  }
  elseif($loanStatus == "old")
  {
    $selectLoans = "SELECT loan.loanId, party.partyName as partyName, loan.loanDate, loan.loanAmount, loan.interest, loan.installmentAmount, loan.installmentDays, installment.isReceived
                      FROM loan
                      JOIN party ON party.PartyId = loan.partyId
                      JOIN installment ON loan.loanId = installment.loanId 
                     WHERE installment.isReceived = 'Y' AND loan.loanDate  BETWEEN  '$start' AND  '$end'
                  GROUP BY loan.loanId ORDER BY loan.loanDate";
    $selectLoansRes = mysql_query($selectLoans);
  }
  elseif($loanStatus == "all")
  {
  	$selectLoans = "SELECT loan.loanId, party.partyName as partyName, loan.loanDate, loan.loanAmount, loan.interest, loan.installmentAmount, loan.installmentDays, installment.isReceived
                      FROM loan
                      JOIN party ON party.PartyId = loan.partyId
                      JOIN installment ON loan.loanId = installment.loanId 
                     WHERE loan.loanDate  BETWEEN  '$start' AND  '$end'
                  GROUP BY loan.loanId ORDER BY loan.loanDate";
    $selectLoansRes = mysql_query($selectLoans);
  }
  while($selectLoansRow = mysql_fetch_array($selectLoansRes))
  {
    $loans[$m]['loanId']            = $selectLoansRow['loanId'];
    $loans[$m]['partyName']         = $selectLoansRow['partyName'];
    $loans[$m]['loanDate']          = date("d-m-Y", strtotime($selectLoansRow['loanDate']));
    $loans[$m]['loanAmount']        = $selectLoansRow['loanAmount'];
    $loans[$m]['interest']          = $selectLoansRow['interest'];
    $loans[$m]['installmentAmount'] = $selectLoansRow['installmentAmount'];
    $loans[$m]['installmentDays']   = $selectLoansRow['installmentDays'];
    // $loans[$m]['guaranter']         = $selectLoansRow['guaranter'];
    $m++;
  }
  $smarty->assign('loans',$loans);
  $smarty->display('selectLoansAjax.tpl');
?>