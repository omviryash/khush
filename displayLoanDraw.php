<?php
include "include/config.inc.php";
$today = date("Y-m-d", strtotime('today'));
$loans = array();
$m= 0 ;
$selectLoans = "SELECT loandraw.loanDrawId, party.partyName, loandraw.drawCardDate, loandraw.drawInstallmentAmount, loandraw.drawInstallmentReceived
                       FROM loandraw
                       JOIN party ON party.PartyId = loandraw.partyId
                       JOIN drawinstallment ON loandraw.loanDrawId = drawinstallment.loanDrawId 
                      WHERE drawinstallment.isReceived = 'N' 
                        AND loandraw.drawCardDate = '$today' 
                   GROUP BY loandraw.loanDrawId ORDER BY loandraw.drawCardDate";
$selectLoansRes = mysql_query($selectLoans);
while($selectLoansRow = mysql_fetch_array($selectLoansRes))
{
  $loans[$m]['loanDrawId']              = $selectLoansRow['loanDrawId'];
  $loans[$m]['partyName']               = $selectLoansRow['partyName'];
  $loans[$m]['drawCardDate']            = date("d-m-Y", strtotime($selectLoansRow['drawCardDate']));
  $loans[$m]['drawInstallmentReceived'] = $selectLoansRow['drawInstallmentReceived'];
  $m++;
}

$smarty->assign('loans',$loans);
$smarty->assign('today',$today);
$smarty->display('displayLoanDraw.tpl');
?>