<?php
include "include/config.inc.php";
$startDateSelect   = date("Y-m-d", strtotime('today'));
$endDateSelect     = date("Y-m-d", strtotime('today'));
$toDate     = isset($_REQUEST['toDateDay']) ? $_REQUEST['toDateYear'] ."-". $_REQUEST['toDateMonth'] ."-". $_REQUEST['toDateDay'] : date("Y-m-d", strtotime('today'));
$party             = array();
$partySelect       = 0;
$account1Select     = 0;
$account2Select     = 0;
$creditCount       = 0;
$debitCount        = 0;
$creditTotal       = 0;
$debitTotal        = 0;
$creditArray       = array();
$debitArray        = array();
$accountIdSelected = "";
$msg = "";

$creditArrayPL     = array();
$debitArrayPL      = array();
$creditArrayAsha   = array();
$debitArrayAsha    = array();
$creditArrayFromParty = array();
$debitArrayFromParty  = array();
$creditArrayPL['dailyLoan']   = 0;
$creditArrayPL['transaction'] = 0;
$creditArrayPL['dailyInterestInterest'] = 0;
$creditArrayPL['monthlyInterest']       = 0;
$creditArrayPL['monthlyBanneInterest']  = 0;
$debitArrayPL['installment']  = 0;
$debitArrayPL['transaction']  = 0;

if(isset($_POST['go']))
{
  $partySelect     = $_REQUEST['party'];
  $account1Select   = -1;
  $account2Select   = $_REQUEST['accountId2'];
  $startDateSelect = "1970-01-01";
  $endDateSelect   = $_REQUEST['toDateYear']."-".$_REQUEST['toDateMonth']."-".$_REQUEST['toDateDay'];
  $startDate  = $_POST['startDateYear']."-".$_POST['startDateMonth']."-".$_POST['startDateDay'];
  $endDate    = $_POST['toDateYear']."-".$_POST['toDateMonth']."-".$_POST['toDateDay'];
  
  //From Loan Table :Start
  if($account1Select == 0 || $account1Select == -1 || $account1Select == 1 || $account1Select == 2 || $account1Select == 15)
  { //0 = All             || -1 = Profit Loss      || 1 = Loan             || 2 = Interest Received || 15 = Ashapura
    $selectloan = "SELECT loanId, loanDate, loanAmount, interest, partyName
                     FROM loan
                LEFT JOIN party ON loan.partyId = party.partyId
                    WHERE loanDate <=  '".$endDate."'";
    if($partySelect > 0)
    {
    	$selectloan .= " AND loan.partyId = ".$_REQUEST['party'];
    }
    $selecttransactionRes = mysql_query($selectloan);
    while($selecttransactionResRow = mysql_fetch_array($selecttransactionRes))
	  {
      if($account1Select == 0 || $account1Select == 1 || $account1Select == 15)
      {
	  	  //Loan : Credit From Ashapura :start
        $creditTotal                         += $selecttransactionResRow['loanAmount'];
        if($selecttransactionResRow['loanDate'] == $endDate)
        {
          $creditArrayPL['dailyLoan']             += $selecttransactionResRow['loanAmount'];
        }
        $creditArray[$creditCount]['amount']  = $selecttransactionResRow['loanAmount'];
        $creditArray[$creditCount]['desc']    = 'Loan To Party : '.$selecttransactionResRow['loanId']." : ".$selecttransactionResRow['partyName'];
        $creditArray[$creditCount]['date']    = date("d-m-Y",strtotime($selecttransactionResRow['loanDate']));
        $creditCount++;
	  	  //Loan : Credit From Ashapura :end
      }
      
      if($account1Select == 0 || $account1Select == -1 || $account1Select == 2)
      {
	  	  //Interest : Credit Interest Received: start
        $creditTotal                        += $selecttransactionResRow['interest'];
        if($selecttransactionResRow['loanDate'] == $endDate)
        {
          $creditArrayPL['dailyLoan']       += $selecttransactionResRow['interest'];
        }
        $creditArray[$creditCount]['amount']  = $selecttransactionResRow['interest'];
        $creditArray[$creditCount]['desc']    = 'Interest From Party : '.$selecttransactionResRow['loanId']." : ".$selecttransactionResRow['partyName'];
        $creditArray[$creditCount]['date']    = date("d-m-Y",strtotime($selecttransactionResRow['loanDate']));
        $creditCount++;
	  	  //Interest : Credit Interest Received: end
	  	}
    }
  }
  //From Loan Table :End
  
  //From Installment Table :Start
  if($account1Select == 15 || $account1Select == 3)
  {
    $selectInterest = "SELECT loanId, installmentDate, installmentAmount, receiveDate
                         FROM installment
                        WHERE receiveDate BETWEEN '".$startDate."' AND  '".$endDate."'
                          AND isReceived = 'Y'";
    if($partySelect > 0)
    {
    	$selectInterest .= " AND loanId IN (SELECT loanId 
    	                                 FROM loan
    	                                WHERE partyId = ".$_REQUEST['party'].")";
    }
    $selectInterestRes = mysql_query($selectInterest);
    while($selectInterestResRow = mysql_fetch_array($selectInterestRes))
	  {
      $selectloan = "SELECT partyName
                       FROM loan
                  LEFT JOIN party ON loan.partyId = party.partyId
                      WHERE loanId = ".$selectInterestResRow['loanId'];
      $selecttransactionRes = mysql_query($selectloan);
      $installmentPartyName = "";
      while($selecttransactionResRow = mysql_fetch_array($selecttransactionRes))
  	  {
  	  	$installmentPartyName = $selecttransactionResRow['partyName'];
  	  }
  	  
    	//Installment : Credit start
      $debitTotal                        += $selectInterestResRow['installmentAmount'];
      if($selectInterestResRow['receiveDate'] == $endDate)
      {
        $debitArrayPL['installment']     += $selectInterestResRow['installmentAmount'];
      }
      $debitArray[$debitCount]['amount']  = $selectInterestResRow['installmentAmount'];
      $debitArray[$debitCount]['desc']    = 'Installment From Party : '.$selectInterestResRow['loanId']." : ".$installmentPartyName;
      $debitArray[$debitCount]['date']    = date("d-m-Y",strtotime($selectInterestResRow['installmentDate']));
      $debitCount++;
  	  //Interest : Credit end
    }
  }
  //From Installment Table :End
  
  //From Transaction Table :Start
	if($partySelect == 0)
	{
    $selecttransaction = "SELECT t.accountIdCr, t.accountIdDr, t.amount, a1.accountName AS accountNameCr, a2.accountName AS accountNameDr, 
                                 a1.inludeInPL AS inludeInPLCr, a2.inludeInPL AS inludeInPLDr,
                                 t.amount, t.transDate, t.narration, interest30days
                            FROM transaction t
                            LEFT JOIN account a1 ON a1.accountId = t.accountIdCr
                            LEFT JOIN account a2 ON a2.accountId = t.accountIdDr
                          WHERE 1 = 1";
    if($account1Select != 0 && $account1Select != -1)
    {
      $selecttransaction .= " AND (   t.accountIdCr =  ".$account1Select."
                                   OR t.accountIdDr =  ".$account1Select."
                                  )";
    }
    if($account2Select != 0 && $account2Select != -1)
    {
      $selecttransaction .= " AND (   t.accountIdCr =  ".$account2Select."
                                   OR t.accountIdDr =  ".$account2Select."
                                  )";
    }
    if($account1Select == -1)
    {
      $selecttransaction .= " AND (   t.accountIdCr IN (SELECT accountId
                                                    FROM account
                                                   WHERE inludeInPL = 'Y')
                                   OR t.accountIdDr IN (SELECT accountId
                                                    FROM account
                                                   WHERE inludeInPL = 'Y')
                                  )";
    }

    $selecttransaction .= " AND t.transDate BETWEEN '".$startDate."' AND  '".$endDate."'";
                        
    $selecttransactionRes = mysql_query($selecttransaction);
    
    while($selecttransactionResRow = mysql_fetch_array($selecttransactionRes))
    {
    	if($selecttransactionResRow['accountIdCr'] == $account1Select && $account1Select != -1)
    	{
        $creditTotal                         += $selecttransactionResRow['amount'];
        if($selecttransactionResRow['transDate'] == $endDate)
        {
          $creditArrayPL['transaction']      += $selecttransactionResRow['amount'];
        }
        $creditArray[$creditCount]['amount']  = $selecttransactionResRow['amount'];
        $creditArray[$creditCount]['desc']    = 'Credit : '.$selecttransactionResRow['accountNameCr']." & Debit : ".$selecttransactionResRow['accountNameDr']
                                                ." : ".$selecttransactionResRow['narration'];
        $creditArray[$creditCount]['date']    = date("d-m-Y",strtotime($selecttransactionResRow['transDate']));
        $creditCount++;
      }
    	elseif($selecttransactionResRow['accountIdDr'] == $account1Select && $account1Select != -1)
    	{
        $debitTotal                        += $selecttransactionResRow['amount'];
        if($selecttransactionResRow['transDate'] == $endDate)
        {
          $debitArrayPL['transaction']     += $selecttransactionResRow['amount'];
        }
        $debitArray[$debitCount]['amount']  = $selecttransactionResRow['amount'];
        $debitArray[$debitCount]['desc']    = 'Credit :: '.$selecttransactionResRow['accountNameCr']." & Debit : ".$selecttransactionResRow['accountNameDr']
                                              ." : ".$selecttransactionResRow['narration'];
        $debitArray[$debitCount]['date']    =  date("d-m-Y",strtotime($selecttransactionResRow['transDate']));
        $debitCount++;
      }
      elseif($account1Select == -1 && $selecttransactionResRow['inludeInPLCr'] == 'Y')
      {
        $debitTotal                        += $selecttransactionResRow['amount'];
        if($selecttransactionResRow['transDate'] == $endDate)
        {
          $debitArrayPL['transaction']     += $selecttransactionResRow['amount'];
        }
        $debitArray[$debitCount]['amount']  = $selecttransactionResRow['amount'];
        $debitArray[$debitCount]['desc']    = 'Credit ::: '.$selecttransactionResRow['accountNameCr']." & Debit : ".$selecttransactionResRow['accountNameDr']
                                                ." : ".$selecttransactionResRow['narration'];
        $debitArray[$debitCount]['date']    = date("d-m-Y",strtotime($selecttransactionResRow['transDate']));
        $debitCount++;
      }
      elseif($account1Select == -1 && $selecttransactionResRow['inludeInPLDr'] == 'Y')
      {
        $creditTotal                         += $selecttransactionResRow['amount'];
        if($selecttransactionResRow['transDate'] == $endDate)
        {
          $creditArrayPL['transaction']      += $selecttransactionResRow['amount'];
        }
        $creditArray[$creditCount]['amount']  = $selecttransactionResRow['amount'];
        $creditArray[$creditCount]['desc']    = 'Credit :::: '.$selecttransactionResRow['accountNameCr']." & Debit : ".$selecttransactionResRow['accountNameDr']
                                              ." : ".$selecttransactionResRow['narration'];
        $creditArray[$creditCount]['date']    = date("d-m-Y",strtotime($selecttransactionResRow['transDate']));
        $creditCount++;
      }
    }
  }
  //From Transaction Table :End
}

$accountArr = array();
if(isset($_REQUEST['accounts']))
{
  $accountIdSelected = $_REQUEST['accounts'];
}
$selAccountQry="SELECT accountId,accountName
                  FROM account
                 ORDER BY orderBy, accountId";
$selAccountQryRes=mysql_query($selAccountQry);
$a1=0;
$a2=0;

//We want to display All option in 2nd combo, not in 1st 
$accountId2Values[$a2]   = '0';
$accountName2[$a2] = 'All';
$a2++;

while($selAccountQryResRow=mysql_fetch_array($selAccountQryRes))
{
	$accountId1Values[$a1]  = $selAccountQryResRow['accountId'];
	$accountName1[$a1]= $selAccountQryResRow['accountName'];
	$a1++;
	$accountId2Values[$a2]  = $selAccountQryResRow['accountId'];
	$accountName2[$a2]= $selAccountQryResRow['accountName'];
	$a2++;
}
$partyArr = array();
$p=0;
$selpartyQry="SELECT partyId,partyName
                FROM party
               ORDER BY partyName";
$selPartyQryRes=mysql_query($selpartyQry);
while($selPartyQryResRow=mysql_fetch_array($selPartyQryRes))
{
	$partyArr['partyId'][$p]   = $selPartyQryResRow['partyId'];
	$partyArr['partyName'][$p] = $selPartyQryResRow['partyName'];
	$p++;
	
}
if(isset($_REQUEST['msg']))
{
  $msg = $_REQUEST['msg'] ;
}
$profitLoss = $creditTotal - $debitTotal ;

//=================================================
//=================================================
//=================================================
//=================================================
//=================================================
$startDateSelect   = date("Y-m-d", strtotime('today'));
$endDateSelect     = date("Y-m-d", strtotime('today'));
$endDate           = date("Y-m-d");
$party             = array();
$partySelect       = 0;
$account1Select     = 0;
$account2Select     = 0;
$creditCount       = 0;
$debitCount        = 0;
$creditTotal       = 0;
$debitTotal        = 0;
$creditArray       = array();
$debitArray        = array();
$accountIdSelected = "";
$msg = "";

if(isset($_POST['go']))
{
  $partySelect     = $_REQUEST['party'];
  $account1Select   = 15;
  $account2Select   = $_REQUEST['accountId2'];
  $startDateSelect = "1970-01-01";
  $endDateSelect   = $_REQUEST['toDateYear']."-".$_REQUEST['toDateMonth']."-".$_REQUEST['toDateDay'];
  $startDate  = $_POST['startDateYear']."-".$_POST['startDateMonth']."-".$_POST['startDateDay'];
  $endDate    = $_POST['toDateYear']."-".$_POST['toDateMonth']."-".$_POST['toDateDay'];
  
  //From Loan Table :Start
  if($account1Select == 0 || $account1Select == -1 || $account1Select == 1 || $account1Select == 2 || $account1Select == 15)
  { //0 = All             || -1 = Profit Loss      || 1 = Loan             || 2 = Interest Received || 15 = Ashapura
    $selectloan = "SELECT loanId, loanDate, loanAmount, interest, partyName
                     FROM loan
                LEFT JOIN party ON loan.partyId = party.partyId
                    WHERE loanDate  BETWEEN '".$startDate."' AND  '".$endDate."'";
    if($partySelect > 0)
    {
    	$selectloan .= " AND loan.partyId = ".$_REQUEST['party'];
    }
    $selecttransactionRes = mysql_query($selectloan);
    while($selecttransactionResRow = mysql_fetch_array($selecttransactionRes))
	  {
      if($account1Select == 0 || $account1Select == 1 || $account1Select == 15)
      {
	  	  //Loan : Credit From Ashapura :start
        $creditTotal                         += $selecttransactionResRow['loanAmount'];
        $creditArray[$creditCount]['amount']  = $selecttransactionResRow['loanAmount'];
        $creditArray[$creditCount]['desc']    = 'Loan To Party : '.$selecttransactionResRow['loanId']." : ".$selecttransactionResRow['partyName'];
        $creditArray[$creditCount]['date']    = date("d-m-Y",strtotime($selecttransactionResRow['loanDate']));
        $creditCount++;
	  	  //Loan : Credit From Ashapura :end
      }
      
      if($account1Select == 0 || $account1Select == -1 || $account1Select == 2)
      {
	  	  //Interest : Credit Interest Received: start
        $creditTotal                        += $selecttransactionResRow['interest'];
        $creditArray[$creditCount]['amount']  = $selecttransactionResRow['interest'];
        $creditArray[$creditCount]['desc']    = 'Interest From Party : '.$selecttransactionResRow['loanId']." : ".$selecttransactionResRow['partyName'];
        $creditArray[$creditCount]['date']    = date("d-m-Y",strtotime($selecttransactionResRow['loanDate']));
        $creditCount++;
	  	  //Interest : Credit Interest Received: end
	  	}
    }
  }
  //From Loan Table :End
  
  //From Installment Table :Start
  if($account1Select == 15 || $account1Select == 3)
  {
    $selectInterest = "SELECT loanId, installmentDate, installmentAmount
                         FROM installment
                        WHERE receiveDate BETWEEN '".$startDate."' AND  '".$endDate."'
                          AND isReceived = 'Y'";
    if($partySelect > 0)
    {
    	$selectInterest .= " AND loanId IN (SELECT loanId 
    	                                 FROM loan
    	                                WHERE partyId = ".$_REQUEST['party'].")";
    }
    $selectInterestRes = mysql_query($selectInterest);
    while($selectInterestResRow = mysql_fetch_array($selectInterestRes))
	  {
      $selectloan = "SELECT partyName
                       FROM loan
                  LEFT JOIN party ON loan.partyId = party.partyId
                      WHERE loanId = ".$selectInterestResRow['loanId'];
      $selecttransactionRes = mysql_query($selectloan);
      $installmentPartyName = "";
      while($selecttransactionResRow = mysql_fetch_array($selecttransactionRes))
  	  {
  	  	$installmentPartyName = $selecttransactionResRow['partyName'];
  	  }
  	  
    	//Installment : Credit start
      $debitTotal                        += $selectInterestResRow['installmentAmount'];
      $debitArray[$debitCount]['amount']  = $selectInterestResRow['installmentAmount'];
      $debitArray[$debitCount]['desc']    = 'Installment From Party : '.$selectInterestResRow['loanId']." : ".$installmentPartyName;
      $debitArray[$debitCount]['date']    = date("d-m-Y",strtotime($selectInterestResRow['installmentDate']));
      $debitCount++;
  	  //Interest : Credit end
    }
  }
  //From Installment Table :End
  
  //From Transaction Table :Start
	if($partySelect == 0)
	{
    $selecttransaction = "SELECT t.accountIdCr, t.accountIdDr, t.amount, a1.accountName AS accountNameCr, a2.accountName AS accountNameDr, 
                                 a1.inludeInPL AS inludeInPLCr, a2.inludeInPL AS inludeInPLDr,
                                 t.amount, t.transDate, t.narration, interest30days
                            FROM transaction t
                            LEFT JOIN account a1 ON a1.accountId = t.accountIdCr
                            LEFT JOIN account a2 ON a2.accountId = t.accountIdDr
                          WHERE 1 = 1";
    if($account1Select != 0 && $account1Select != -1)
    {
      $selecttransaction .= " AND (   t.accountIdCr =  ".$account1Select."
                                   OR t.accountIdDr =  ".$account1Select."
                                  )";
    }
    if($account2Select != 0 && $account2Select != -1)
    {
      $selecttransaction .= " AND (   t.accountIdCr =  ".$account2Select."
                                   OR t.accountIdDr =  ".$account2Select."
                                  )";
    }
    if($account1Select == -1)
    {
      $selecttransaction .= " AND (   t.accountIdCr IN (SELECT accountId
                                                    FROM account
                                                   WHERE inludeInPL = 'Y')
                                   OR t.accountIdDr IN (SELECT accountId
                                                    FROM account
                                                   WHERE inludeInPL = 'Y')
                                  )";
    }

    $selecttransaction .= " AND t.transDate BETWEEN '".$startDate."' AND  '".$endDate."'";
                        
    $selecttransactionRes = mysql_query($selecttransaction);
    
    while($selecttransactionResRow = mysql_fetch_array($selecttransactionRes))
    {
    	if($selecttransactionResRow['accountIdCr'] == $account1Select && $account1Select != -1)
    	{
        $creditTotal                         += $selecttransactionResRow['amount'];
        $creditArray[$creditCount]['amount']  = $selecttransactionResRow['amount'];
        $creditArray[$creditCount]['desc']    = 'Credit : '.$selecttransactionResRow['accountNameCr']." & Debit : ".$selecttransactionResRow['accountNameDr']
                                                ." : ".$selecttransactionResRow['narration'];
        $creditArray[$creditCount]['date']    = date("d-m-Y",strtotime($selecttransactionResRow['transDate']));
        $creditCount++;
      }
    	elseif($selecttransactionResRow['accountIdDr'] == $account1Select && $account1Select != -1)
    	{
        $debitTotal                        += $selecttransactionResRow['amount'];
        $debitArray[$debitCount]['amount']  = $selecttransactionResRow['amount'];
        $debitArray[$debitCount]['desc']    = 'Credit :: '.$selecttransactionResRow['accountNameCr']." & Debit : ".$selecttransactionResRow['accountNameDr']
                                              ." : ".$selecttransactionResRow['narration'];
        $debitArray[$debitCount]['date']    =  date("d-m-Y",strtotime($selecttransactionResRow['transDate']));
        $debitCount++;
      }
      elseif($account1Select == -1 && $selecttransactionResRow['inludeInPLCr'] == 'Y')
      {
        $debitTotal                        += $selecttransactionResRow['amount'];
        $debitArray[$debitCount]['amount']  = $selecttransactionResRow['amount'];
        $debitArray[$debitCount]['desc']    = 'Credit ::: '.$selecttransactionResRow['accountNameCr']." & Debit : ".$selecttransactionResRow['accountNameDr']
                                                ." : ".$selecttransactionResRow['narration'];
        $debitArray[$debitCount]['date']    = date("d-m-Y",strtotime($selecttransactionResRow['transDate']));
        $debitCount++;
      }
      elseif($account1Select == -1 && $selecttransactionResRow['inludeInPLDr'] == 'Y')
      {
        $creditTotal                         += $selecttransactionResRow['amount'];
        $creditArray[$creditCount]['amount']  = $selecttransactionResRow['amount'];
        $creditArray[$creditCount]['desc']    = 'Credit :::: '.$selecttransactionResRow['accountNameCr']." & Debit : ".$selecttransactionResRow['accountNameDr']
                                              ." : ".$selecttransactionResRow['narration'];
        $creditArray[$creditCount]['date']    = date("d-m-Y",strtotime($selecttransactionResRow['transDate']));
        $creditCount++;
      }
    }
  }
  //From Transaction Table :End
}

$accountArr = array();
if(isset($_REQUEST['accounts']))
{
  $accountIdSelected = $_REQUEST['accounts'];
}
$selAccountQry="SELECT accountId,accountName
                  FROM account
                 ORDER BY orderBy, accountId";
$selAccountQryRes=mysql_query($selAccountQry);
$a1=0;
$a2=0;

//We want to display All option in 2nd combo, not in 1st 
$accountId2Values[$a2]   = '0';
$accountName2[$a2] = 'All';
$a2++;

while($selAccountQryResRow=mysql_fetch_array($selAccountQryRes))
{
	$accountId1Values[$a1]  = $selAccountQryResRow['accountId'];
	$accountName1[$a1]= $selAccountQryResRow['accountName'];
	$a1++;
	$accountId2Values[$a2]  = $selAccountQryResRow['accountId'];
	$accountName2[$a2]= $selAccountQryResRow['accountName'];
	$a2++;
}
$partyArr = array();
$p=0;
$selpartyQry="SELECT partyId,partyName
                FROM party
               ORDER BY partyName";
$selPartyQryRes=mysql_query($selpartyQry);
while($selPartyQryResRow=mysql_fetch_array($selPartyQryRes))
{
	$partyArr['partyId'][$p]   = $selPartyQryResRow['partyId'];
	$partyArr['partyName'][$p] = $selPartyQryResRow['partyName'];
	$p++;
	
}
if(isset($_REQUEST['msg']))
{
  $msg = $_REQUEST['msg'] ;
}
$ashapura = $debitTotal - $creditTotal ;


//=================================================
//=================================================
//=================================================
//=================================================
//=================================================


$totalPendingAmount = 0;
$grandTotal = 0;

///////////////////////

	$installmentQuery = "SELECT installmentAmount
	                    FROM installment
	                   WHERE loanId IN (SELECT loanId FROM loan WHERE loanDate <= '".$toDate."')
	                     AND (receiveDate > '".$toDate."'
	                      OR receiveDate = '0000-00-00')
	                    ";
	$installmentQueryRes = mysql_query($installmentQuery);
	
	while($installmentQueryResRow = mysql_fetch_array($installmentQueryRes))
	{
	  $totalPendingAmount += $installmentQueryResRow['installmentAmount'];
	}

///////////////////////
///////////////////////
$selLoanDetail = "SELECT loan.loanId, loan.installmentReceived, loan.installmentAmount, installmentDays
                    FROM loan
                   WHERE installmentDays > installmentReceived
                    ";
$selLoanDetailRes = mysql_query($selLoanDetail);

$i = 0;
while($selLoanDetailRow = mysql_fetch_array($selLoanDetailRes))
{
	
  $loanDetailRow[$i]['loanId']              = $selLoanDetailRow['loanId'];
  $loanDetailRow[$i]['installmentDays']     = $selLoanDetailRow['installmentDays'];
  $loanDetailRow[$i]['installmentReceived'] = $selLoanDetailRow['installmentReceived'];
  $loanDetailRow[$i]['installmentAmount']   = $selLoanDetailRow['installmentAmount'];
  
  $i++;
}

//=================================================
$dailyinterestQuery = "SELECT loanId, loanDate, loanAmount, interest
                         FROM dailyinterest
                        WHERE loanDate <= '".$toDate."'";
$dailyinterestQueryRes = mysql_query($dailyinterestQuery) or die(mysql_error());
while($dailyinterestQueryRow = mysql_fetch_array($dailyinterestQueryRes))
{
	$ashapura -= $dailyinterestQueryRow['loanAmount'];
	$totalPendingAmount += $dailyinterestQueryRow['loanAmount'];
	$profitLoss         += $dailyinterestQueryRow['interest'];
  if($dailyinterestQueryRow['loanDate'] == $endDate)
  {
    $creditArrayPL['dailyInterestInterest'] += $dailyinterestQueryRow['interest'];
  }
}
//=================================================

//=================================================
$monthlyQuery = "SELECT loanId, loanDate, loanAmount, interest
                         FROM monthly
                        WHERE loanDate <= '".$toDate."'";
$monthlyRes = mysql_query($monthlyQuery) or die(mysql_error());
while($monthlyRow = mysql_fetch_array($monthlyRes))
{
	$ashapura -= $monthlyRow['loanAmount'];
	$totalPendingAmount += $monthlyRow['loanAmount'];
	$profitLoss         += $monthlyRow['interest'];
  if($monthlyRow['loanDate'] == $endDate)
  {
    $creditArrayPL['monthlyInterest'] += $monthlyRow['interest'];
  }
}
//=================================================

//=================================================
$monthlybanneQuery = "SELECT loanId, loanDate, loanAmount, interest
                         FROM monthlybanne
                        WHERE loanDate <= '".$toDate."'";
$monthlybanneRes = mysql_query($monthlybanneQuery) or die(mysql_error());
while($monthlybanneRow = mysql_fetch_array($monthlybanneRes))
{
	$ashapura -= $monthlybanneRow['loanAmount'];
	$totalPendingAmount += $monthlybanneRow['loanAmount'];
}

$monthlybanneQuery = "SELECT accountIdDr, amount, transDate
                         FROM transaction
                        WHERE monthlyBanneId > 0
                          AND transDate <= '".$toDate."'
                          AND (accountIdDr = 32
                               OR accountIdDr = 33)";
$monthlybanneRes = mysql_query($monthlybanneQuery) or die(mysql_error());
$monthlybanne32 = 0;  // 32 = AshapuraOrProfitLoss
$monthlybanne33 = 0;  // 33 = takeMeToProfitLoss
$monthlybanne32Today = 0;
$monthlybanne33Today = 0;
while($monthlybanneRow = mysql_fetch_array($monthlybanneRes))
{
	if($monthlybanneRow['accountIdDr'] == 32)
	{
		$monthlybanne32 += $monthlybanneRow['amount'];
		if($monthlybanneRow['transDate'] == $endDate)
		{
		  $monthlybanne32Today += $monthlybanneRow['amount'];
		}
	}
	elseif($monthlybanneRow['accountIdDr'] == 33)
	{
		$monthlybanne33 += $monthlybanneRow['amount'];
		if($monthlybanneRow['transDate'] == $endDate)
		{
		  $monthlybanne33Today += $monthlybanneRow['amount'];
		}
	}
}
if($monthlybanne32 >= $monthlybanne33)
{
	$profitLoss += $monthlybanne33;
	$creditArrayPL['monthlyBanneInterest'] += $monthlybanne33Today;
	$ashapura += ($monthlybanne32 - $monthlybanne33);
	$totalPendingAmount -= ($monthlybanne32 - $monthlybanne33);
}
else
{
	$profitLoss += $monthlybanne32;
	$creditArrayPL['monthlyBanneInterest'] += $monthlybanne32Today;
	$totalPendingAmount += ($monthlybanne33 - $monthlybanne32);
}

//=================================================
//=================================================
//=================================================
$monthlyTransQuery = "SELECT accountIdDr, amount, transDate
                        FROM transaction
                       WHERE monthlyId > 0
                         AND transDate <= '".$toDate."'
                         AND accountIdDr = 15";
$monthlyTransQueryRes = mysql_query($monthlyTransQuery) or die(mysql_error());
$monthlyMuddalReturned = 0;
while($monthlyTransQueryRow = mysql_fetch_array($monthlyTransQueryRes))
{
  $monthlyMuddalReturned += $monthlyTransQueryRow['amount'];
	$totalPendingAmount -= $monthlyTransQueryRow['amount'];
}
//=================================================
//=================================================
$dailyInterestTransQuery = "SELECT accountIdDr, amount, transDate
                        FROM transaction
                       WHERE dailyinterestId > 0
                         AND transDate <= '".$toDate."'
                         AND accountIdDr = 15";
$dailyInterestTransRes = mysql_query($dailyInterestTransQuery) or die(mysql_error());
$dailyInterestMuddalReturned = 0;
while($dailyInterestTransResRow = mysql_fetch_array($dailyInterestTransRes))
{
  $dailyInterestMuddalReturned += $dailyInterestTransResRow['amount'];
	$totalPendingAmount -= $dailyInterestTransResRow['amount'];
}
//=================================================
//=================================================
//=================================================

$grandTotal = $profitLoss + $ashapura + $totalPendingAmount;
$plAsha     = $profitLoss + $ashapura ;
$ashapuraAndParty = $ashapura + $totalPendingAmount ;

$smarty->assign('toDateSelect',$toDate);
$smarty->assign('profitLoss',$profitLoss);
$smarty->assign('ashapura',$ashapura);
$smarty->assign('plAsha',$plAsha);
$smarty->assign('totalPendingAmount',$totalPendingAmount);
$smarty->assign('ashapuraAndParty',$ashapuraAndParty);
$smarty->assign('grandTotal',$grandTotal);
$smarty->assign('creditArrayPL',$creditArrayPL);
$smarty->assign('debitArrayPL',$debitArrayPL);
$smarty->display('summary.tpl');
?>