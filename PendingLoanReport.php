<?php
include "include/config.inc.php";
$today = date("Y-m-d");
$msg="";
$documentId = array();
$documentName = array();
$m=0;
$loanIdFind = "";
$searchDate = '';
if(isset($_POST['search'])){
	if(isset($_POST['fromDateDay']) && isset($_POST['fromDateMonth']) && isset($_POST['fromDateYear']))
	{
		$searchDate = date('Y-m-d',strtotime($_POST['fromDateYear'].'-'.$_POST['fromDateMonth'].'-'.$_POST['fromDateDay']));
	}
}else{
	$searchDate = date('Y-m-d');
}	
	$today 		= date('d-m-Y',strtotime($searchDate));
	$select = "select party.partyName,loan.loanId,loan.interest, loan.loanAmount,
				case when 1=1 then (
					SELECT count(0) FROM loan_details where loan_details.loanId = loan.loanId and loan_details.recevied = 0 and loan_details.due_date < '".$searchDate."'
				) end as pending_installment,
				case when 1=1 then (
					select sum(transactionnew.transactionAmount)
					FROM transactionnew where transactionnew.loanId = loan.loanId
					AND transactionnew.creditDebit = 'Credit'
					group by transactionnew.loanId
				) end as amount_recevied
				FROM loan
				INNER JOIN party ON party.partyId = loan.partyId 
				where loan.loanTypeId = 4";
$selLoanDetailRes 	= mysql_query($select);
$total_interest 	= 0;
$total_loan_amount 	= 0;
$selLoanDetailRowData = array();
while($selLoanDetailRow = mysql_fetch_array($selLoanDetailRes))
{
	$selLoanDetailRow['pending_interest']		= 0;
	$selLoanDetailRow['pending_loan_amount']	= $selLoanDetailRow['loanAmount'] - $selLoanDetailRow['amount_recevied'];
	$pending_loan_amount 						= $selLoanDetailRow['loanAmount'] - $selLoanDetailRow['amount_recevied'];
	if (($selLoanDetailRow['pending_installment'] > 0) && $pending_loan_amount > 0) {
		$pending_interest_amount 				= ($selLoanDetailRow['pending_installment'] * $pending_loan_amount / 100);
		$selLoanDetailRow['pending_interest']	= $selLoanDetailRow['pending_installment'] * $pending_interest_amount;
	}
	$selLoanDetailRow['total_pending']  		= $selLoanDetailRow['pending_interest'] + $selLoanDetailRow['pending_loan_amount'];
	$selLoanDetailRowData[] = $selLoanDetailRow;
	$total_interest 	+= $selLoanDetailRow['pending_interest'];
	$total_loan_amount 	+= $selLoanDetailRow['pending_loan_amount'];
}
$total 							= $total_interest + $total_loan_amount;
$array['pending_loan_amount'] 	= $total_loan_amount;
$array['pending_interest'] 		= $total_interest;
$array['total_pending']			= $total;
$array['partyName']				= '';
$selLoanDetailRowData[] 		= $array;

/* echo '<pre>';
print_r($selLoanDetailRowData); */

//print_r($selLoanDetailRowData);
$smarty->assign("documentId",$documentId);
$smarty->assign("documentName",$documentName);
$smarty->assign("today",$today);
$smarty->assign("selLoanDetailRowData",$selLoanDetailRowData);
$smarty->assign('msg',$msg);
$smarty->display('PendingLoanReport.tpl');
?>