ALTER TABLE  `transaction` ADD  `pendingAmount` DOUBLE NOT NULL DEFAULT  '0' AFTER  `amount`;
UPDATE  `transaction` SET pendingamount = amount WHERE interest30days >0;

CREATE TABLE IF NOT EXISTS `dailyinterest` (
  `loanId` int(11) NOT NULL,
  `partyId` int(11) default NULL,
  `loanDate` date default NULL,
  `loanAmount` double default NULL,
  `interest` double default NULL,
  `installmentAmount` double default NULL,
  `installmentDays` int(11) default NULL,
  `guaranterId` int(11) default NULL,
  `installmentReceived` int(11) default NULL,
  PRIMARY KEY  (`loanId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `monthly` (
  `loanId` int(11) NOT NULL,
  `partyId` int(11) default NULL,
  `loanDate` date default NULL,
  `loanAmount` double default NULL,
  `interest` double default NULL,
  `installmentAmount` double default NULL,
  `installmentDays` int(11) default NULL,
  `guaranterId` int(11) default NULL,
  `installmentReceived` int(11) default NULL,
  PRIMARY KEY  (`loanId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `monthlybanne` (
  `loanId` int(11) NOT NULL,
  `partyId` int(11) default NULL,
  `loanDate` date default NULL,
  `loanAmount` double default NULL,
  `interest` double default NULL,
  `installmentAmount` double default NULL,
  `installmentDays` int(11) default NULL,
  `guaranterId` int(11) default NULL,
  `installmentReceived` int(11) default NULL,
  PRIMARY KEY  (`loanId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;