--
-- Table structure for table `loanamount`
--

CREATE TABLE `loanamount` (
  `id` int(11) NOT NULL,
  `party` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `amount_type` varchar(20) DEFAULT NULL,
  `add_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loanamount`
--
--
-- Indexes for table `loanamount`
--
ALTER TABLE `loanamount`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `loanamount`
--
ALTER TABLE `loanamount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;