<html>
	<head>
	  <link rel="stylesheet" href="css/style1.css">
	</head>
	{block name=head}
  {/block}
	<body>
  	<ul class="menu">
	    <li><a href="#">Master Entry</a>
	    	<ul>
				<li><a href="index.php">Party</a></li>
				<li><a href="agentEntry.php">Agent</a></li>
				<li><a href="loan.php">Loan</a></li>
	    	</ul>
	    </li>
		<li><a href="#">Loan Detail</a>
	    	<ul>
	    		<li><a href="loanDetail1.php">Loan Detail</a></li>
				<li><a href="loanMonthly.php">Loan Detail (Monthly)</a></li>
			</ul>
		</li>
	    <!--<li><a href="account.php">Account</a></li>-->
	    <li>
			<a href="transactionNew.php">Transaction</a>
	    	<!--<ul>
	        <li><a href="transaction.php">Transaction</a></li>
	        <li><a href="transactionBanne.php">Banne</a></li>
	        <li><a href="transactionMonthly.php">Monthly</a></li>
	        <li><a href="transactionDInterest.php">DailyInterest</a></li>
	      </ul>-->
	    </li>
	    <li><a href="#">Display</a>
	      <ul>
	        <li><a href="displayClient.php">Parties</a></li>
	        <!--<li><a href="displayLoanDraw.php">Loan Draw</a></li>-->
	        <!--<li><a href="dispalyTransaction.php">Transactions</a></li>-->
	        <!--<li><a href="loandrawreceived.php">DrawReceived</a></li>-->
	        <li><a href="loanSummaryNew.php">LoanSummary</a></li>
			<li><a href="dayBook.php">Day Book</a></li>
			<li><a href="dayBook.php?format=old">Day Book Old Format</a></li>
	        <!--<li><a href="summary.php">Summary</a></li>-->
	      </ul>
	    </li>
	    <!--<li><a href=""> Draw </a>
	      <ul>
	        <li><a href="loanDraw.php">Draw Entry</a></li>
	        <li><a href="drawLoansDetail.php">Draw Detail</a></li>
	        <li><a href="drawWinner.php">Draw</a></li>
	      </ul>
	    </li>-->
	    <li><a href="#">Report</a>
	      <ul>
	        <!--<li><a href="loanEndDay.php">Daily Loan End Day</a></li>-->
	        <!--<li><a href="pendingInstallment.php">Daily Loan Pending</a></li>-->
            <li><a href="PendingLoanReport.php">Pending Loan Report</a></li>
            <li><a href="ProfitLoss.php">Profit Report (NEW)</a></li>
            <li><a href="agentReport.php">Agent Report</a></li>
	      </ul>
	    </li>
	    <li><a href="#">FD</a>
	      <ul>
	        <li><a href="fdAmountEntry.php">Entry</a></li>
			    <li><a href="fdClientList.php">List</a></li>
	      </ul>
	    </li>
	    <li><a href="#">Check Me</a>
	      <ul>
	        <li><a href="displayLoan.php">Loans</a></li>
	        <li><a href="loanreceived.php">LoanReceived</a></li>
	        <li><a href="loanDetail2.php">LoanDetailAll</a></li>
	      </ul>
	    </li>
	    <li><a href="pending.php">Pending</a>
	  
		</li>
	    
      
      <!--<li><a href="interestToPay.php">Interest To Pay</a></li>-->
    </ul>
    {block name=body}
    {/block}
	</body>
</html>