{extends file="./link.tpl"}
{block name="head"}
<link rel="stylesheet" href="css/fin3.css" />
<style type="text/css">
</style>
{/block}
{block name=body}	
<script src="js/jquery.min.js" ></script>
 <script type="text/javascript">
    function showPartyform()
    {
	  var dataString = $("#amount_party").val();
	  if(dataString=='0'){
	    $.ajax(
	    {
	      type: "GET",
	      url: "enterNewPartyAjax.php",
	      data: "partyId="+dataString,
	      success:function(data)
	      {
			$('#party_form').show();
	        $('.partyData').html(data);
	      }
	    });
	  }
	  else{
		  $('#party_form').hide();
	      $('.partyData').html('');
	  }
    }
 </script>
<form name="expenceForm" id="expenceForm" method="post">
  <center>
  <table align="center" cellpadding="0" cellspacing="0">		
    
    <h1 align="center">FD Interest Entry</h1>
    <tr>
    	<!--<th>FD ID :</th>
    	<td>
        <select name="fd_id" id="fd_id">
            <option value="">-- Select FD ID --</option>
			{html_options values=$arrFdID.fd_id  output=$arrFdID.fd_id}
        </select>
        </td>-->
		<th>Party Name:</th>
		<td>
			 <select name="party_id" id="party_id">
				<option value="">-- Select Party --</option>
				{html_options values=$arrParty.partyId  output=$arrParty.partyName}
			</select>
		</td>
    </tr>
	<tr>
		<th>Loan:</th>
		<td>
			 <select name="loan_id" id="loan_id">
				<option value="">-- Select Loan --</option>
			</select>
		</td>
	</tr>
	<tr id="party_form" style="display:none;">
		<td class="partyData" colspan="2">
		</td>
    </tr>
    <tr>
    	<th>Interest Amount :</th>
    	<td><input type="text" name="int_amount" placeholder="Interest Amount" id="int_amount" required="required"></td>
    </tr>
	<tr>
    	<th>Lidha / Aapya :</th>
    	<td>
			<select name="lidha_aapya" id="lidha_aapya">
				<option value="">-- Select --</option>
				<option value="lidha">Lidha</option>
				<option value="aapya">Aapya</option>
			</select>
        </td>
    </tr>
	<tr>
    	<th>Interest Date :</th>
    	<td>{html_select_date prefix='curDate' field_order="DmY" month_format="%m" start_year='-5' end_year='+5' time=$today}</td>
    </tr>
	<tr>
      <td colspan="2" align="center">
        <input type="reset" name="reset" id="reset" value="Reset">
        <input type="submit" name="sub" id="sub" value="SAVE">
      </td>       
    </tr>
  </table>
</center>
</form>
<script>

$( document ).ready(function(){
	$('#party_id').change(function(){
		$.ajax({
			url: "fdInterestEntry.php",
			data: { post_key: "get_loan_detail", party_id: $(this).val() },
			method: "POST"
		})
		.done(function( html ) {
			var option_of_loan = JSON.parse(html);
			$('#loan_id').html('<option value="">-- Select Loan --</option>');
			if(option_of_loan != ''){
				for(i=0;i<option_of_loan['loanId'].length;i++){
					$('#loan_id').append('<option value="'+option_of_loan['loanId'][i]+'">'+option_of_loan['loanAmount'][i]+'</option>');
				}
			}
		});
	});
});
</script>
{/block}
