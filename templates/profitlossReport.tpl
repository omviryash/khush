{extends file="./link.tpl"}
{block name="head"}
  <script src="js/jquery.min.js" ></script>
  <script type="text/javascript">
  </script>
{/block}

{block name=body}
<form name="profitlossSummary" method="get">
  <table align="center" cellpadding="5" cellspacing="5" border="1">
  	<h1 align="center">Profit/Loss Statement</h1>
  	<tr>
    	<td>From Date :
  			{html_select_date prefix='curFromDate' field_order="DmY" month_format="%m" start_year='-5' end_year='+5' time=$curFromDate}&nbsp;&nbsp;&nbsp;&nbsp;
  		</td>
  		<td>To Date :
  			{html_select_date prefix='curToDate' field_order="DmY" month_format="%m" start_year='-5' end_year='+5' time=$curToDate}&nbsp;&nbsp;&nbsp;&nbsp;
  		</td>
        <td>
  	    	<input type="submit" value="GO...!" />
  	    </td>
  		<!--<td>Till Date :
  			{html_select_date prefix='curDate' field_order="DmY" month_format="%m" start_year='-5' end_year='+5' time=$curDate}&nbsp;&nbsp;&nbsp;&nbsp;
  	  </td>
  	  <td>
  	    <input type="submit" value="GO...!" />
  	  </td>-->
  	</tr>
  </table>
  <center>
    <table border="1" cellspacing="0" cellpadding="3">
      <tr>
        <th align="center">Date</th>
        <th align="center">Income</th>
        <th align="center">Expense</th>
        <th align="center">Profit/Loss</th>
      </tr>
      {section name=sec loop=$profitlossArray}
      <tr>
        <td align="center">{$profitlossArray[sec]['date']|date_format:"%d-%m-%Y"}</td>
        <td align="right"><a href=profitlossDetail.php?type=income&date={$profitlossArray[sec]['date']|date_format:"%d-%m-%Y"} onclick="window.open(this.href, '_blank', 'left=20,top=20,width=700,height=500,toolbar=1,resizable=0'); return false;">{$profitlossArray[sec]['income'] + $profitlossArray[sec]['income1']}</a></td>
        <td align="right"><a href=profitlossDetail.php?type=expense&date={$profitlossArray[sec]['date']|date_format:"%d-%m-%Y"} onclick="window.open(this.href, '_blank', 'left=20,top=20,width=700,height=500,toolbar=1,resizable=0'); return false;">{$profitlossArray[sec]['expence'] + $profitlossArray[sec]['expence1']}</a></td>
        <td align="center">{$profitlossArray[sec]['profitloss']}</td>
      </tr>
      {/section}
      <tr>
      	<th colspan="3" align="right">Total Profit/Loss: </th>
      	<th align="right">{$totalamount}</th>
      </tr>
    </table>
  </center>
</form>
{/block}
