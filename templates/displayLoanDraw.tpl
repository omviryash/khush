{extends file="./link.tpl"}
{block name=head}
<script src="js/jquery.min.js" ></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="media/js/jquery.js" type="text/javascript"></script>
<script src="media/js/jquery.dataTables.js" type="text/javascript"></script>
<style type="text/css">
  @import "media/css/demo_table_jui.css";
  @import "media/themes/smoothness/jquery-ui-1.8.4.custom.css";
input[type="submit"], input[type="reset"] {
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;  
    border-radius: 10px;
    }

    input[type="button"] {
    float: right;
      margin: 2em 1em 0 1em;
      width: 5em;
      padding: .5em;
      border: 1px solid #666;
      -moz-border-radius: 10px;
      -webkit-border-radius: 10px;  
      border-radius: 10px;
      -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      color: #fff;
      background: #0a0;
      font-size: 1em;
      line-height: 1em;
      font-weight: bold;
      opacity: .7;
      -webkit-appearance: none;
      -moz-transition: opacity .5s;
      -webkit-transition: opacity .5s;
      -o-transition: opacity .5s;
      transition: opacity .5s;
    }
    
    input[type="submit"]:hover,
    input[type="submit"]:active,
    input[type="reset"]:hover,
    input[type="reset"]:active {
      cursor: pointer;
      opacity: 1;
    }
    
    input[type="submit"]:active, input[type="button"]:active {
      color: #333;
      background: #eee;
      -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
      -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
      box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
    }
    
    input[type="reset"] {
      background: #f33;
    }	
</style>
<style>
  *{
     font-family: arial;
   }
   
</style>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function(){
    $('#datatables').dataTable({
      "sPaginationType":"full_numbers",
      "aaSorting":[[2, "desc"]],
      "bJQueryUI":true
    });
  });
  function selectLoans()
  {
  	
  	  var startDay=document.frm.StartDateDay.value;
      var startMonth=document.frm.StartDateMonth.value;
      var startYear=document.frm.StartDateYear.value;
      
      var startDate=startYear+'-'+startMonth+'-'+startDay;

      var endDay=document.frm.EndDateDay.value;
      var endMonth=document.frm.EndDateMonth.value;
      var endYear=document.frm.EndDateYear.value;
      
      var endDate=endYear+'-'+endMonth+'-'+endDay;
      
      
  	loanStatus = $("#loanStatus").val();
    $.ajax({
      url:'selectDrawLoansAjax.php',
      dataType:'HTML',
      data: 'loanStatus='+loanStatus+'&startDatePass='+startDate+'&endDatePass='+endDate,
      type:'GET',
      success:function(msg)
      {
        $('#displayDiv').html(msg);
         $('#datatables').dataTable({
           "sPaginationType":"full_numbers",
           "aaSorting":[[2, "desc"]],
           "bJQueryUI":true
         });
      }
    });
  }
</script>
{/block}
{block name=body}
<br /><br /><br /><br /><br /><br /><br />
<form name="frm" id="frm" method="post">
<table align="center">
  <tr>
    <td>
      <select name="loanStatus" id="loanStatus" onchange="selectLoans(this);">
        <option value="current">Current</option>
        <option value="old">Old</option>
        <option value="all">All</option>
      </select>
       To:
      {html_select_date prefix='StartDate' id="StartDate" time=$today start_year='-5'
       end_year='+5' field_order="DMY" month_format="%m"}
       From:
      {html_select_date prefix='EndDate' time=$today start_year='-5'
      end_year='+5' field_order="DMY" month_format="%m"}
    </td>
    <td>
      <input type="button" name="go" id="go" value="Go" onclick="selectLoans()"> 
    </td>
  </tr>
</table>
</form>
<div id="displayDiv">
  <table id="datatables" class="display">
    <thead>
      <tr>
        <th>Delete</th>
        <th>Card No</th>
        <th>partyName</th>
        <th>Draw Card Date</th>
        <th>Installment Amount</th>
        <th>Installment Months</th>
        <th>Draw Installment Recived </th>
        <th>Draw Installment Panding </th>
      </tr>
    </thead>
    <tbody id="insData">
      {section name=rec loop=$loans} 
        <tr>
          <td><a href="DrawLoansDelete.php?loanId={$loans[rec].loanDrawId}">Delete</a></td>
          <td>{$loans[rec].loanDrawId}</td>
          <td>{$loans[rec].partyName}</td>
          <td align="center">{$loans[rec].drawCardDate}</td>
          <td align="right">1000</td>
          <td align="right">20</td>
          <td align="right">{$loans[rec].drawInstallmentReceived}</td>
          <td align="right">20-{$loans[rec].drawInstallmentReceived}</td>
        </tr>
      {/section}
    </tbody>
  </table>
</div>
{/block}