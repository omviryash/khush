<table id="datatables" class="display">
    <thead>
      <tr>
        <th>Delete</th>
        <th>Card No</th>
        <th>partyName</th>
        <th>Draw Card Date</th>
        <th>Installment Amount</th>
        <th>Installment Months</th>
        <th>Draw Installment Recived </th>
        <th>Draw Installment Panding </th>
      </tr>
    </thead>
    <tbody id="insData">
      {section name=rec loop=$loans} 
        <tr>
          <td><a href="DrawLoansDelete.php?loanId={$loans[rec].loanDrawId}">Delete</a></td>
          <td>{$loans[rec].loanDrawId}</td>
          <td>{$loans[rec].partyName}</td>
          <td align="center">{$loans[rec].drawCardDate}</td>
          <td align="right">1000</td>
          <td align="right">20</td>
          <td align="right">{$loans[rec].drawInstallmentReceived}</td>
          <td align="right">{20-$loans[rec].drawInstallmentReceived}</td>
        </tr>
      {/section}
    </tbody>
  </table>