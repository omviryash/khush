{extends file="./link.tpl"}
{block name="head"}
  <script src="js/jquery.min.js" ></script>
  <script type="text/javascript">
  </script>
{/block}

{block name=body}
<form name="dayBook" method="get">
  <table align="center" cellpadding="5" cellspacing="5" border="1">
  	<h1 align="center">Day Book</h1>
  	<tr>
		<td>From Date :
  			{html_select_date prefix='fromDate' field_order="DmY" day_value_format="%02d" month_format="%m" start_year='2011' end_year='+5' time=$fromDate}&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
		<td>To Date :
  			{html_select_date prefix='toDate' field_order="DmY" day_value_format="%02d" month_format="%m" start_year='2011' end_year='+5' time=$toDate}&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
		<td>
			<input type="submit" value="GO...!" />
		</td>
  	</tr>
  </table>
  </br></br>
  <center>
    <table border="1" cellspacing="0" cellpadding="4">
      <tr>
        <th align="center">Date</th>
        <th align="center">Particulars</th>
        <th align="center">Credit</th>
        <th align="center">Debit</th>
        <th align="center">Balance</th>
      </tr>
	  {section name=sec loop=$transactionArr}
	  <tr>
        <td align="center">{$transactionArr[sec]['transactionDate']}</th>
        <td align="left">{$transactionArr[sec]['note']}</td>
        <td align="right">{if $transactionArr[sec]['creditDebit'] == 'Credit'} {$transactionArr[sec]['transactionAmount']} {/if}</td>
        <td align="right">{if $transactionArr[sec]['creditDebit'] == 'Debit'} {$transactionArr[sec]['transactionAmount']} {/if}</td>
        <td align="right">{$transactionArr[sec]['balance']}</td>
      </tr>
	  {/section}
    </table>
  </center>
</form>
{/block}
