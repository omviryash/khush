{extends file="./link.tpl"}
{block name=head}
<style type="text/css">
  	input[type="submit"], input[type="reset"] {
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;  
    border-radius: 10px;
    }

    input[type="submit"], input[type="reset"] {
    
    
    float: right;
      margin: 2em 1em 0 1em;
      width: 8em;
      padding: .5em;
      border: 1px solid #666;
      -moz-border-radius: 10px;
      -webkit-border-radius: 10px;  
      border-radius: 10px;
      -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      color: #fff;
      background: #0a0;
      font-size: 1em;
      line-height: 1em;
      font-weight: bold;
      opacity: .7;
      -webkit-appearance: none;
      -moz-transition: opacity .5s;
      -webkit-transition: opacity .5s;
      -o-transition: opacity .5s;
      transition: opacity .5s;
    }
    
    input[type="submit"]:hover,
    input[type="submit"]:active,
    input[type="reset"]:hover,
    input[type="reset"]:active {
      cursor: pointer;
      opacity: 1;
    }
    
    input[type="submit"]:active, input[type="button"]:active {
      color: #333;
      background: #eee;
      -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
      -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
      box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
    }
    
    input[type="reset"] {
      background: #f33;
    }	
  </style>  
  
  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script type="text/javascript">

		function fetch_select(val,loanId)
		{
		   $.ajax({
			 type: 'post',
			 url: 'fetch_data.php',
			 data: {
			   get_option:val,
			   loanId:loanId,
			   transactionFor:$('#transactionFor').val()
			 },
			 success: function (response) {
			   	document.getElementById("new_select").innerHTML=response; 
				document.getElementById("installments").innerHTML="";
			 }
		   });
		}
		function fetch_installments(val)
		{
		   $.ajax({
			 type: 'post',
			 url: 'fetch_data.php',
			 data: {
			   get_installments:1,
			   loan_id:val,
			   
			 },
			 success: function (response) {
				document.getElementById("installments").innerHTML=response;
			 }
		   });
		}
		$(document).ready(function(){
			var loanId = {$loanId};
			if (loanId > 0) {
				$("#transactionFor").val(2);
				$("#transactionFor").trigger("onchange");
			}
		});
		function fetch_installmentsfd(val)
		{
		   $.ajax({
			 type: 'post',
			 url: 'fetch_data.php',
			 data: {
			   get_installmentsfd:1,
			   fd_id:val,
			   
			 },
			 success: function (response) {
				document.getElementById("installments").innerHTML=response;
			 }
		   });
		}
		$(document).ready(function(){
			var loanId = {$loanId};
			if (loanId > 0) {
				$("#transactionFor").val(2);
				$("#transactionFor").trigger("onchange");
			}
		});
		var temp = setInterval(doStuff, 100);
		function doStuff()
		{
			var loanId = {$loanId};
			if (loanId > 0) {
				$("#loanId"+loanId).trigger("onchange");
				clearInterval(temp);
			}
		}
		function checkboxVal() 
		{
			var t = 0;
			var j = 0;
			
			for (var i = 0; i < accountForm.elements.length; i++ ) 
			{
				if (accountForm.elements[i].type == 'checkbox') 
				{
					j++;
					if (accountForm.elements[i].checked == true) 
					{
						t = 1;
					}
				}
			}
			if(t != 1 && j != 0)
			{
				alert("Select Month For Which You Want to Pay Interest");
				event.preventDefault()
			}
        }
  </script>
{/block}
{block name=body}	
</br>
</br>
<form name="accountForm" id="accountForm" method="post">
<table style="margin-left:100px;" valign="top">
	<tr>
		<td valign="top">
		  <h1 align="center">Account Entry</h1>
			<table valign="top">
			  <tr>
				<th align="right">Transacton : </th>
				<td>
				  <select name="transactionFor" id="transactionFor" required onchange="fetch_select(this.value,{$loanId});">
					<option value="">-- Select --</option>
					{html_options values=$transactionFor.accountId output=$transactionFor.accountName}
				  </select>
				</td>
			  </tr>
			  <tr>
				<th align="right">Transacton Type : </th>
				<td>
				  <select name="transactionType" id="transactionType" required>
					<option value="" {if $creditDebit == "" } selected="selected" {/if}>-- Select --</option>
					<option value="Credit" {if $creditDebit == "Credit" } selected="selected" {/if}>Credit</option>
					<option value="Debit" {if $creditDebit == "Debit" } selected="selected" {/if}>Debit</option>
				  </select>
				</td>
			  </tr>
			  <tr>
				<th align="right">Date :</th>
				<td>{html_select_date prefix="transDate" start_year ="-0" end_year="+2" field_order="DMY" month_format="%m" day_value_format="%02d"}</td>
			  </tr>
			  <tr>
				<th align="right">Amount :</th>
				<td><input type="text" required=required  name="transAmount" autofocus id="transAmount" placeholder="Amount"> 
				</td>
			  </tr>
			  <tr>
			  </tr>
			  <tr>
				<th align="right" valign="center">Note :</th>
				<td>
				  <textarea cols="17" id="note" name="note" rows="5"></textarea>  	
				</td>
			  </tr>
			  <tr>
				<td colspan="2" align="center">
				  <input type="submit" name="Submit" id="Submit" value="SAVE" onclick="checkboxVal();">
				</td>       
			  </tr>
			</table>
		</td>
		<td id="new_select" valign="top">
			
		</td>
		<td id="installments" valign="top">
			
		</td>
	</tr>
</table>
</form>
{/block}