{extends file="./link.tpl"}
{block name=head}
<style type="text/css">
  	input[type="submit"], input[type="reset"] {
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;  
    border-radius: 10px;
    }

    input[type="submit"], input[type="reset"] {
    
    
    float: right;
      margin: 0em 1em 0 1em;
	  width: 5em;
      padding: .5em;
      border: 1px solid #666;
      -moz-border-radius: 10px;
      -webkit-border-radius: 10px;  
      border-radius: 10px;
      -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      color: #fff;
      background: #0a0;
      font-size: 1em;
      line-height: 1em;
      font-weight: bold;
      opacity: .7;
      -webkit-appearance: none;
      -moz-transition: opacity .5s;
      -webkit-transition: opacity .5s;
      -o-transition: opacity .5s;
      transition: opacity .5s;
    }
    
    input[type="submit"]:hover,
    input[type="submit"]:active,
    input[type="reset"]:hover,
    input[type="reset"]:active {
      cursor: pointer;
      opacity: 1;
    }
    
    input[type="submit"]:active, input[type="button"]:active {
      color: #333;
      background: #eee;
      -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
      -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
      box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
    }
    
    input[type="reset"] {
      background: #f33;
    }	
  </style>  
  
  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  
{/block}
{block name=body}	
</br>
</br>
<form name="accountForm" id="accountForm" method="post">
 <table align="center" border="2" style="border-collapse: collapse;">
 <tr>
	<td align="center" colspan="2"><h2>Agent Name {$agentName} , Commission Rate {$agent_list[0]['commission_per']} %</h2></td>
 </tr>
 <tr>
	<td align="center"><h3 align="center">Credit</h1></td>
	<td align="center"><h3 align="center">Debit</h1></td>
 </tr>
 <tr>
 <td>
	<table border="1" cellspacing="0" cellpadding="3">
    <tr>
        <th align="center">Date</th>
        <th align="center">Particulars</th>
        <th align="center">Amount</th>
	</tr>
      {assign var="TotalCommision" value="0"}
      {assign var="TotalPaid" value="0"}
      {assign var="TotalPending" value="0"}
      {if count($agent_list) gt 0}
  	  {section name="sec" loop=$agent_list}
	  <tr>
		<td align="right">{$agent_list[sec]['loanDate']}</td>
		<td align="right">Loan Id : {$agent_list[sec]['loanId']} Party Name : {$agent_list[sec]['partyId']} Loan Type : {$agent_list[sec]['loanTypeId']} Loan Amount : {$agent_list[sec]['loanAmount']}</td>
		<td align="right">{$agent_list[sec]['commission']}</td>
      </tr> 
	   {assign var="TotalCommision" value="`$TotalCommision + $agent_list[sec]['commission']`"}
      {/section}
  	  <tr>
		<td align="right"><b>Total</b></td>
		<td align="right">&nbsp;</td>
		<td align="right">{$TotalCommision}</td>
      </tr> 
	{else}
      <tr>
        <td colspan="3">No Records found for selected criteria.</td>
      </tr>
      {/if}
    </table>
</td>
<td>
	<table border="1" cellspacing="0" cellpadding="3">
      <tr>
        <th align="center">Date</th>
        <th align="center">Particulars</th>
        <th align="center">Amount</th>
      </tr>
      {if count($paidCommissionArr) gt 0}
  	  {section name="sec" loop=$paidCommissionArr}
	  <tr>
		<td align="right">{$paidCommissionArr[sec]['transactionDate']}</td>
		<td align="right">{$paidCommissionArr[sec]['note']}</td>
		<td align="right">{$paidCommissionArr[sec]['transactionAmount']}</td>
      </tr> 
	   {assign var="TotalPaid" value="`$TotalPaid + $paidCommissionArr[sec]['transactionAmount']`"}
      {/section}
  	  <tr>
		<td align="right"><b>Total</b></td>
		<td align="right">&nbsp;</td>
		<td align="right">{$TotalPaid}</td>
      </tr> 
	
	  {else}
      <tr>
        <td colspan="3">No Records found for selected criteria.</td>
      </tr>
      {/if}
    </table>
	</td>
</tr>
<tr>
        <th align="left"><h3>Pending Commission</h3></th>
		{assign var="TotalPending" value="`$TotalCommision - $TotalPaid`"}
	    <td align="right"><h3>{$TotalPending}</h3></td>
</tr>
</table>
</form>
{/block}