{extends file="./link.tpl"}
{block name=head}
<script src="js/jquery.min.js" ></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="media/js/jquery.js" type="text/javascript"></script>
<script src="media/js/jquery.dataTables.js" type="text/javascript"></script>
<style type="text/css">
  @import "media/css/demo_table_jui.css";
  @import "media/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<style>
  *{
     font-family: arial;
   }
</style>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function(){
    $('#datatables').dataTable({
      "sPaginationType":"full_numbers",
      "aaSorting":[[2, "desc"]],
      "bJQueryUI":true
    });
  });
  function myFunction()
{
  if(confirm("You Want To Remove This Record")== true)
	{
		return true;
	}
	else
	{
		return false;
	}
}
</script>
{/block}
{block name=body}
<br /><br /><br /><br /><br /><br /><br /><br />
<div>
  <center>
    <font color = "red">{$msg}</font>
  </center>
</div>
<div>
  <table id="datatables" class="display">
    <thead>
      <tr>
        <th>party Name</th>
        <th>Address</th>
        <th>Phone1</th>
        <th>Phone2</th>
        <th>DocumentsId</th>
        <th>OtherInfo</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
      {section name=rec loop=$parties} 
        <tr>
          <td>{$parties[rec].partyName}</td>
          <td>{$parties[rec].address}</td>
          <td>{$parties[rec].phone1}</td>
          <td>{$parties[rec].phone2}</td>
          <td>{$parties[rec].documentsId}</td>
          <td>{$parties[rec].otherInfo}</td>
          <td align="center"><a href="partyDelete.php?partyId={$parties[rec].partyId}" onclick="return myFunction();" class="link"> <img src="./images/deleteIco.png"></a></td>
        </tr>
      {/section}
    </tbody>
  </table>
</div>
{/block}