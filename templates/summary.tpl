{extends file="./link.tpl"}
{block name="head"}
<style type="text/css">
  	input[type="submit"], input[type="reset"] {
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;  
    border-radius: 10px;
    }
    input[type="submit"], input[type="reset"] {
    float: right;
      margin: 2em 1em 0 1em;
      width: 5em;
      padding: .5em;
      border: 1px solid #666;
      -moz-border-radius: 10px;
      -webkit-border-radius: 10px;  
      border-radius: 10px;
      -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      color: #fff;
      background: #0a0;
      font-size: 1em;
      line-height: 1em;
      font-weight: bold;
      opacity: .7;
      -webkit-appearance: none;
      -moz-transition: opacity .5s;
      -webkit-transition: opacity .5s;
      -o-transition: opacity .5s;
      transition: opacity .5s;
    }
    input[type="submit"]:hover,
    input[type="submit"]:active,
    input[type="reset"]:hover,
    input[type="reset"]:active {
      cursor: pointer;
      opacity: 1;
    }
    input[type="submit"]:active, input[type="button"]:active {
      color: #333;
      background: #eee;
      -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
      -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
      box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
    }
    input[type="reset"] {
      background: #f33;
    }	
  </style>  
{/block}
{block name=body}
<br><br><br><br>
<form name="frm" id="frm" method="POST">
<input type="hidden" name="party" value="0">
<input type="hidden" name="accountId1" value="-1">
<input type="hidden" name="accountId2" value="0">
<input type="hidden" name="startDateDay" value="01">
<input type="hidden" name="startDateMonth" value="01">
<input type="hidden" name="startDateYear" value="1970">
<table align="center">
  <tr>
    <td>
      Up To:
      {html_select_date prefix='toDate' time=$toDateSelect start_year='-5' end_year='+5' field_order="DMY" month_format="%m" day_value_format="%02d"}
    </td>
    <td><input type="submit" name="go" id="go" value="Go"></td>
  </tr>
</table>
</form>
<br><br><br><br>
<table align="center" cellspacing="0" cellpadding="6" border="1">
	<tr>
		<td valign="top">Profit / Loss
		</td>
		<td valign="top">Ashapura
		</td>
		<td valign="top">Total
		</td>
		<td valign="top">From Party
		</td>
		<td valign="top">Ashapura + Party
		</td>
  </tr>
	<tr>
		<td align="right" valign="top">{$profitLoss}
		</td>
		<td align="right" valign="top">{$ashapura}
		</td>
		<td align="right" valign="top">{$plAsha}
		</td>
		<td align="right" valign="top">{$totalPendingAmount}
		</td>
		<td align="right" valign="top">{$ashapuraAndParty}
		</td>
  </tr>
</table>
<table align="center" cellspacing="1" cellpadding="1">
<tr>
	<td>Grand Total:  {$grandTotal}</td>
</tr>
</table>
<br><br>
<table align="center" cellspacing="0" cellpadding="6" border="1">
	<tr>
		<td valign="top" COLSPAN="6">{$toDateSelect|date_format:"%d-%m-%Y"}
		</td>
	</tr>
	<tr>
		<td align="center">Profit / Loss Cr
		</td>
		<td valign="top">Profit / Loss Dr
		</td>
		<td valign="top">Ashapura
		</td>
		<td valign="top">Total
		</td>
		<td valign="top">From Party
		</td>
		<td valign="top">Ashapura + Party
		</td>
  </tr>
	<tr>
		<td align="right" valign="top">
			<table cellpadding="3" cellspacing="0" border="1">
				{foreach key=creditArrayKey item=creditArrayItem from=$creditArrayPL}
				<tr>
					<td align="right">{$creditArrayKey} : </td>
					<td align="right">{$creditArrayItem}</td>
				</tr>
				{/foreach}
			</table>
		</td>
		<td align="right" valign="top">
			<table cellpadding="3" cellspacing="0" border="1">
				{foreach key=debitArrayKey item=debitArrayItem from=$debitArrayPL}
				<tr>
					<td align="right">{$debitArrayKey} : </td>
					<td align="right">{$debitArrayItem}</td>
				</tr>
				{/foreach}
			</table>
		</td>
		<td align="right" valign="top">{$ashapura}
		</td>
		<td align="right" valign="top">{$plAsha}
		</td>
		<td align="right" valign="top">{$totalPendingAmount}
		</td>
		<td align="right" valign="top">{$ashapuraAndParty}
		</td>
  </tr>
</table>
<table align="center" cellspacing="1" cellpadding="1">
<tr>
	<td>Grand Total:  {$grandTotal}</td>
</tr>
</table>
{/block}