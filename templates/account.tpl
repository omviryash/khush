{extends file="./link.tpl"}
{block name="head"}
<link rel="stylesheet" href="css/fin3.css" />
<script src="js/jquery.min.js" ></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="media/js/jquery.js" type="text/javascript"></script>
<script src="media/js/jquery.dataTables.js" type="text/javascript"></script>
<style type="text/css">
  @import "media/css/demo_table_jui.css";
  @import "media/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<style>
  *{
     font-family: arial;
   }
   input[type=text], 
   textarea {
     width: 200px;
   }
</style>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function(){
    $('#datatables').dataTable({
      "sPaginationType":"full_numbers",
      "aaSorting":[[2, "desc"]],
      "bJQueryUI":true
    });
  });
</script>
{/block}
{block name=body}	
<form name="accountForm" id="accountForm" method="post" action="account.php">
  <h1 align="center">Account Entry</h1>
  <center>
  <table>		
    <tr>
      <th align="right">Account Name :</th>
      <td><input type="text" required=required  name="accountName" autofocus id="accountName" placeholder="Account Name"></td>
    </tr>
    <tr>
      <th align="right" >Other Information :</th>
      <td>
        <textarea cols="17" id="otherInfo" name="otherInfo" rows="5"></textarea>  	
      </td>
    </tr>
    <tr>
      <th align="right">Include In Profit Loss :</th>
      <td><input type="checkbox" name="profitLoss" id="profitLoss" value="y"/> <td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <input type="reset" name="reset" id="reset" value="Reset">
        <input type="submit" name="sub" id="sub" value="SAVE">
      </td>       
    </tr>
  </table>
</center>
</form>
<br /><br /><br />


<div id="displayDiv">
  <h1 align="center">Account List</h1>
  <table id="datatables" class="display">
    <thead>
      <tr>
        <th>Delete</th>
        <th>Account Name</th>
        <th>Other Information</th>
      </tr>
    </thead>
    
    <tbody>
      {section name=sec loop=$account}
      <tr>
        <td align="center"><a onclick="return confirm('Are u Sure to Delete');" href="deleteAccount.php?accountId={$account[sec].accountId}"><img src="./images/deleteIcon.png" width="20px" height="20px"></a></td>
        <td>{$account[sec].accountName}</td>
        <td>{$account[sec].otherInfo}</td>
      </tr>
      {/section}
    </tbody>
  </table>
</div>
{/block}