{extends file="./link.tpl"}
{block name="head"}
  <script src="js/jquery.min.js" ></script>
  <script type="text/javascript">
  </script>
{/block}

{block name=body}
<form name="dayBook" method="get">
  <table align="center" cellpadding="5" cellspacing="5" border="1">
  	<h1 align="center">Loan Detail</h1>
  </table>
  </br></br>
  <center>
  <table border="0" cellspacing="0" cellpadding="4">
  <tr>
  <td vAlign="top">
    <table border="1" cellspacing="0" cellpadding="4">
      <tr>
        <th align="center">Date</th>
		<th align="center">Party Name</th>
		<th align="center">FD Amount</th>
		<th align="center">FD Interest Rate</th>
		<th align="center">Fd Date</th>
        <th align="center">Particulars</th>
        <th align="center">Credit</th>
        <th align="center">Debit</th>
        <th align="center">Balance</th>
      </tr>
	  {section name=sec loop=$fdLoanDetailArr}
	  {if $fdLoanDetailArr[sec]['transactionForId'] == 5}
	  <tr>
		<td align="center">{$fdLoanDetailArr[sec]['transactionDate']}</th>
		<td align="center">{$fdLoanDetailArr[sec]['partyName']}</th>
		<td align="center">{$fdLoanDetailArr[sec]['fd_amount']}</th>
		<td align="center">{$fdLoanDetailArr[sec]['fd_interest_rate']}</th>
		<td align="center">{$fdLoanDetailArr[sec]['fd_date']}</th>
        <td align="left">{$fdLoanDetailArr[sec]['note']}</td>
        <td align="right">{if $fdLoanDetailArr[sec]['creditDebit'] == 'Credit'} {$fdLoanDetailArr[sec]['transactionAmount']} {/if}</td>
        <td align="right">{if $fdLoanDetailArr[sec]['creditDebit'] == 'Debit'} {$fdLoanDetailArr[sec]['transactionAmount']} {/if}</td>
        <td align="right">{$fdLoanDetailArr[sec]['balance']}</td>
      </tr>
	  {/if}
	  {/section}
    </table>
  </td>
  <td>
	{assign var="interestTotal" value="0"}
	<table border="1" cellspacing="0" cellpadding="4">
      <tr>
        <th align="center">Date</th>
        <th align="center">Interest Paid</th>
      </tr>
	  {section name=sec loop=$fdLoanDetailArr}
	{if $fdLoanDetailArr[sec]['transactionForId'] == 6}
	  <tr>
        <td align="center">{$fdLoanDetailArr[sec]['transactionDate']}</td>
        <td align="right">{if $fdLoanDetailArr[sec]['creditDebit'] == 'Debit'} {$fdLoanDetailArr[sec]['transactionAmount']} {/if}</td>
		{assign var="interestTotal" value="`$interestTotal+$fdLoanDetailArr[sec]['transactionAmount']`"}
      </tr>
	  {/if}
	  {/section}
	  <tr>
		<th align="center">Total</th>
		<td align="right">{$interestTotal}</td>
      </tr>
    </table>
  </tr>
  </table>
  </center>
</form>
{/block}
