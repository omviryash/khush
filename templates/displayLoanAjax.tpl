{section name=rec loop=$loans} 
  <tr>
  	<td>{$loans[rec].loanId}</td>
    <td>{$loans[rec].partyName}</td>
    <td>{$loans[rec].loanDate}</td>
    <td>{$loans[rec].loanAmount}</td>
    <td>{$loans[rec].interest}</td>
    <td>{$loans[rec].installmentAmount}</td>
    <td>{$loans[rec].installmentDays}</td>
  </tr>
{/section}