{extends file="./link.tpl"}
{block name="head"}
  <script src="js/jquery.min.js" ></script>
  <script type="text/javascript">
  </script>
{/block}

{block name=body}
<form name="loanSummary" method="get">
  <table align="center" cellpadding="5" cellspacing="5" border="1">
  	<h1 align="center">Loan Pending Installment To Current Date Party Wise</h1>
  	<tr>
  		<td>Party :
  		  <select name="partyId" id="partyId" onchange="this.form.submit();">
          <option value="0">All Party</option>
          {html_options values=$partyArr.partyId  output=$partyArr.partyName selected=$selectedParty}
        </select>
  		</td>
  		<!--<td>Till Date :
  			{html_select_date prefix='curDate' field_order="DmY" month_format="%m" start_year='-5' end_year='+5' time=$curDate}&nbsp;&nbsp;&nbsp;&nbsp;
  	  </td>
  	  <td>
  	    <input type="submit" value="GO...!" />
  	  </td>-->
  	</tr>
  </table>
  <center>
    <table border="1" cellspacing="0" cellpadding="3">
      <tr>
        <th align="center">Loan Type</th>
        <th align="center">Party Name</th>
        <th align="center">Loan No</th>
        <th align="center">Loan Date</th>
        <th align="center">Loan Amount</th>
        <th align="center">Pending Installment</th>
      </tr>
      {section name=sec loop=$loanArray}
      <tr>
        <td align="center">{$loanArray[sec]['loanType']}</td>
        <td align="center">
        	{if $selectedParty > 0} 
        	  {$onePartyName}
        	{else} 
            {$loanArray[sec]['partyName']} 
        	{/if}
        </td>
        <td align="center">{$loanArray[sec]['loanId']}</td>
        <td align="center">{$loanArray[sec]['loanDate']|date_format:"%d-%m-%Y"}</td>
        <td align="right">
        	{$loanArray[sec]['loanAmount']}
        	{assign var="loanAmountTotal" value="`$loanAmountTotal+$loanArray[sec]['loanAmount']`"}
        </td>
		<td align="center">{$loanArray[sec]['pendingInstallment']}</td>
        
      </tr>
      {/section}
    </table>
  </center>
</form>
{/block}
