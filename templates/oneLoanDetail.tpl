{extends file="./link.tpl"}
{block name="head"}
  <script src="js/jquery.min.js" ></script>
  <script type="text/javascript">
  </script>
{/block}

{block name=body}
<form name="dayBook" method="get">
  <table align="center" cellpadding="5" cellspacing="5" border="1">
  	<h1 align="center">Loan Detail</h1>
  </table>
  </br></br>
  <center>
  <table border="0" cellspacing="0" cellpadding="4">
  <tr>
  <td vAlign="top">
    <table border="1" cellspacing="0" cellpadding="4">
	  <tr>
		<td colspan="5" align="center">
			Party Name : {$partyName}
		</td>
	  </tr>
      <tr>
        <th align="center">Date</th>
        <th align="center">Particulars</th>
        <th align="center">Credit</th>
        <th align="center">Debit</th>
        <th align="center">Balance</th>
      </tr>
	  {section name=sec loop=$oneLoanDetailArr}
	  {if $oneLoanDetailArr[sec]['transactionForId'] == 1}
	  <tr>
        <td align="center">{$oneLoanDetailArr[sec]['transactionDate']}</th>
        <td align="left">{$oneLoanDetailArr[sec]['note']}</td>
        <td align="right">{if $oneLoanDetailArr[sec]['creditDebit'] == 'Credit'} {$oneLoanDetailArr[sec]['transactionAmount']} {/if}</td>
        <td align="right">{if $oneLoanDetailArr[sec]['creditDebit'] == 'Debit'} {$oneLoanDetailArr[sec]['transactionAmount']} {/if}</td>
        <td align="right">{$oneLoanDetailArr[sec]['balance']}</td>
      </tr>
	  {/if}
	  {/section}
    </table>
  </td>
  <td>
	{assign var="interestTotal" value="0"}
	<table border="1" cellspacing="0" cellpadding="4">
      <tr>
        <th align="center">Date</th>
        <th align="center">Interest Received</th>
      </tr>
	  {section name=sec loop=$oneLoanDetailArr}
	{if $oneLoanDetailArr[sec]['transactionForId'] == 2}
	  <tr>
        <td align="center">{$oneLoanDetailArr[sec]['transactionDate']}</td>
        <td align="right">{if $oneLoanDetailArr[sec]['creditDebit'] == 'Credit'} {$oneLoanDetailArr[sec]['transactionAmount']} {/if}</td>
		{assign var="interestTotal" value="`$interestTotal+$oneLoanDetailArr[sec]['transactionAmount']`"}
      </tr>
	  {/if}
	  {/section}
	  <tr>
		<th align="center">Total</th>
		<td align="right">{$interestTotal}</td>
      </tr>
    </table>
  </tr>
  </table>
  </center>
</form>
{/block}
