{extends file="./link.tpl"}
{block name="head"}
	<script src="js/jquery.min.js" ></script>
	<script src="media/js/jquery.js" type="text/javascript"></script>
	<script src="media/js/jquery.dataTables.js" type="text/javascript"></script>
	<style type="text/css">
		@import "media/css/demo_table_jui.css";
		@import "media/themes/smoothness/jquery-ui-1.8.4.custom.css";
		*{
			font-family: arial;
		}
	</style>

<script type="text/javascript" charset="utf-8">
	$(document).ready(function(){
		$('#datatables').dataTable({
			"sPaginationType":"full_numbers",
			"aaSorting":[[2, "desc"]],
			"bJQueryUI":true
		});
	});
	function myFunction()
	{
		if(confirm("You Want To Remove This Record")== true)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
</script>

{/block}

{block name=body}
<form name="fdClientList" method="get">
  <table align="center" cellpadding="5" cellspacing="5" border="1">
  	<h1 align="center">FD List</h1>
  	<!--<tr>
  		<td>Party :
			<select name="partyId" id="partyId" onchange="this.form.submit();">
			  <option value="0">All Party</option>
			  {html_options values=$arrParty.partyId  output=$arrParty.partyName selected=$selectedParty}
			</select>
  		</td>
  	</tr>-->
  </table>
  <center>
    <table id="datatables" class="display">
		<thead>
			<tr>
				<th align="center">Click For Detail</th>
				<th align="center">FD ID</th>
				<th align="center">Party Name</th>
				<th align="center">Lidha</th>
				<th align="center">FD Interest Rate</th>
				<th align="center">FD Date</th>
			</tr>
		</thead>	
		<tbody>
			{section name=sec loop=$loanArray}
			<tr>
				<td align="center"><a href="fdLoanDetail.php?loanId={$loanArray[sec]['fd_id']}">Detail</a></td>
				<td align="center">{$loanArray[sec]['fd_id']}</td>
				<td align="left">{$loanArray[sec]['partyName']}</td>
				<td align="right">{$loanArray[sec]['fd_amount']}</td>
				<td align="right">{$loanArray[sec]['fd_interest_rate']}</td>
				<td align="center">{$loanArray[sec]['fd_date']|date_format:"%d-%m-%Y"}</td>
			</tr>
		    {/section}
		</tbody>
    </table>
  </center>
</form>
{/block}
