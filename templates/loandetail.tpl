{extends file="./link.tpl"}
{block name="head"}
  <script src="js/jquery.min.js" ></script>
  <style type="text/css">
  	input[type="submit"], input[type="reset"] {
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;  
    border-radius: 10px;
    }

    input[type="submit"], input[type="reset"] {
    
    
    float: right;
      margin: 2em 1em 0 1em;
      width: 8em;
      padding: .5em;
      border: 1px solid #666;
      -moz-border-radius: 10px;
      -webkit-border-radius: 10px;  
      border-radius: 10px;
      -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      color: #fff;
      background: #0a0;
      font-size: 1em;
      line-height: 1em;
      font-weight: bold;
      opacity: .7;
      -webkit-appearance: none;
      -moz-transition: opacity .5s;
      -webkit-transition: opacity .5s;
      -o-transition: opacity .5s;
      transition: opacity .5s;
    }
    
    input[type="submit"]:hover,
    input[type="submit"]:active,
    input[type="reset"]:hover,
    input[type="reset"]:active {
      cursor: pointer;
      opacity: 1;
    }
    
    input[type="submit"]:active, input[type="button"]:active {
      color: #333;
      background: #eee;
      -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
      -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
      box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
    }
    
    input[type="reset"] {
      background: #f33;
    }	
  </style>  
  <script src="js/jquery-ui.js"></script>
  <script type="text/javascript">
    function InstallmentAmount(obj)
    {
    	if(obj.checked == true)
    	{
    	  var ckval=obj.value;
    	  var chid=obj.id;
    	  $("#Receive"+chid).val(ckval);	
    	}
    	else
    	{
          var chid=obj.id;
    	  $("#Receive"+chid).val("");
    	}
    }
  </script>
{/block}
{block name=body}
    <form name="installmentForm" id="installmentForm" method="post">
      <table align="center" cellpadding="5" cellspacing="5" border="1">
      	<h1 align="center">Loan Detail</h1>
      	<tr>
      		<td>Loan Id:<input type="text" value="{if $loanIdVar!= 0}{$loanIdVar}{/if}" name="loanId" id="loanId"></td>
      		<td>
      		  <select name="partyNameSel" id="partyNameSel">
              <option selected=selected value="0">-- All --</option>
              {html_options values=$selectPartyId  output=$selectPartyName selected=$selectedParty}
            </select>
      		</td>
      	  <th>Receive Payment For : </th>
      	  <td>{html_select_date prefix='curDate' field_order="DmY" month_format="%m" start_year='-5' end_year='+5' time=$today}&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="go" id="go" value="Go"></td>
      	</tr>
      </table>
    </form>
    <form name="installmentFormDetail" id="installmentFormDetail" method="post">
      <table align="center" cellpadding="5" cellspacing="0" border="1">
      	<tr>
      		<input type="hidden" value={$today} name="installmentDate">
      		<th>Loan No.</th>
      		<th>Party</th>
      		<th>Installment Date</th>
      		<th>Installment Amount</th>
      		<th>Received</th>
      	</tr>
      	{section name=user loop=$installmentArr}
      	<tr>
      		<td>{$installmentArr[user].loanId} : {$installmentArr[user].installmentNo}</td>
      		<td>{$installmentArr[user].partyName}</td>
      		<td>{$installmentArr[user].installmentDate}</td>
      		<td align="right">{$installmentArr[user].installmentAmount}</td>
      		<td>
      		  <input type="checkbox" name="Received" id="{$installmentArr[user].installmentId}" onclick="InstallmentAmount(this)" value="{$installmentArr[user].installmentAmount}">
      		  <input type="text" name="installmentAmount[]" id="Receive{$installmentArr[user].installmentId}" >
      		  <input type="hidden" name="InstallmentAmountVal" id="InstallmentAmountVal" value="{$installmentArr[user].installmentAmount}">
      		  <input type="hidden" name="InstallmentDate[]" id="InstallmentDate" value="{$installmentArr[user].installmentDate}">
      		  <input type="hidden" name="InstallmentsID[]" id="InstallmentsID" value="{$installmentArr[user].installmentId}">
      		  <input type="hidden" name="loanId[]" id="loanId" value="{$installmentArr[user].loanId}">
      		</td>
      	</tr>
      	{/section}
      	<tr>
      		<td colspan="4" align="right">{$toatalAmount}</td>
      	  <td colspan="4" align="right"><input type="submit" name="sub" id="sub" value="SAVE"></td>
      	</tr>
      </table>
    </form>
    
{/block}