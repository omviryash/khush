<table cellpadding="2" cellspacing="0" border="1" >
<tr>
<td colspan="2">
	<input type="hidden" name="partyIdOld" value="{$partyId}">
  <h1 align="left">Party Entry</h1>
</td>
</tr>
<tr>
<th align="left">Party Name :</th>
<td><input type="text" required=required  name="partyName" autofocus id="partyName" value="{$partyName}"></td>
</tr>
<tr>
  <th align="left" valign="top">Address :</th>
  <td><textarea cols="17" id="address" name="address" rows="5" >{$address}</textarea></td>
</tr>
<tr>
  <th align="left">City :</th>
  <td><input type="text" name="city" id="city" value={$city}></td>
</tr>
<tr>
  <th align="left">Phone1 :</th>
  <td><input type="text" name="phone1" id="phone1" value={$phone1}></td>
</tr>
<tr>
  <th align="left">Phone2 :</th>
  <td><input type="text" name="phone2" id="phone2" value={$phone2}></td>
</tr>
<tr>
  <th align="left" valign="top">Documents :</th>
  <td>
    {html_checkboxes name='documents' values=$documentId output=$documentName  selected=$documentsId separator='<br />'}
  </td>
</tr>
<tr>
  <th align="left">Other Information :</th>
  <td>
    <textarea cols="17" id="otherInfo" name="otherInfo" rows="5">{$otherInfo}</textarea>  	
  </td>
</tr>
</table>