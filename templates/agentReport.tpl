{extends file="./link.tpl"}
{block name=head}
<style type="text/css">
  	input[type="submit"], input[type="reset"] {
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;  
    border-radius: 10px;
    }

    input[type="submit"], input[type="reset"] {
    
    
    float: right;
      margin: 0em 1em 0 1em;
	  width: 5em;
      padding: .5em;
      border: 1px solid #666;
      -moz-border-radius: 10px;
      -webkit-border-radius: 10px;  
      border-radius: 10px;
      -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      color: #fff;
      background: #0a0;
      font-size: 1em;
      line-height: 1em;
      font-weight: bold;
      opacity: .7;
      -webkit-appearance: none;
      -moz-transition: opacity .5s;
      -webkit-transition: opacity .5s;
      -o-transition: opacity .5s;
      transition: opacity .5s;
    }
    
    input[type="submit"]:hover,
    input[type="submit"]:active,
    input[type="reset"]:hover,
    input[type="reset"]:active {
      cursor: pointer;
      opacity: 1;
    }
    
    input[type="submit"]:active, input[type="button"]:active {
      color: #333;
      background: #eee;
      -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
      -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
      box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
    }
    
    input[type="reset"] {
      background: #f33;
    }	
  </style>  
  
  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  
{/block}
{block name=body}	
</br>
</br>
<form name="accountForm" id="accountForm" method="post">
  <table align="center">
  	<h1 align="center">Agent Report</h1>
  	<tr>
    	<td>Search Date :
  			{html_select_date prefix='curFromDate' field_order="DmY" month_format="%m" start_year='-5' end_year='+5' time=$curFromDate}
  		</td>
  		<td>
  	    	<input type="submit" value="Search" />
  	    </td>
  	</tr>
  </table>
  <center style="padding-top:20px;">
    <table border="1" cellspacing="0" cellpadding="3">
      <tr>
        <th align="center">Agent Name</th>
        <th align="center">Agent Commission</th>
        <th align="center">Agent Paid</th>
        <th align="center">Total Payable Amont</th>
      </tr>
	  {assign var="TotalCommision" value="0"}
      {assign var="TotalPaid" value="0"}
      {assign var="TotalPending" value="0"}
	  {assign var="Pending" value="0"}
      {if count($agent_list) gt 0}
  	   {foreach from=$agent_list item=value}
        <tr>
          <td><a href="agentReportDetail.php?agentId={$value['agentId']}">{$value['agentName']}</a></td>
          <td align="right">{$value['totalCommission']}</td>
          <td align="right">{$value['paidCommission']}</td>
		  {assign var="Pending" value="`$value['totalCommission'] - $value['paidCommission']`"}
		  <td align="right">{$Pending}</td>
        </tr>
		{assign var="TotalCommision" value="`$TotalCommision + $value['totalCommission']`"}
		{assign var="TotalPaid" value="`$TotalPaid + $value['paidCommission']`"}
		{assign var="TotalPending" value="`$TotalPending + $Pending`"}
  	  {/foreach}
      <tr>
        <td>Total</td>
        <td align="right">{$TotalCommision}</td>
        <td align="right">{$TotalPaid}</td>
        <td align="right">{$TotalPending}</td>
      </tr>
      {else}
      <tr>
        <td colspan="4">No Records found for selected criteria.</td>
      </tr>
      {/if}
	</table>
  </center>
</form>
{/block}