{extends file="./link.tpl"}
{block name="head"}
<link rel="stylesheet" href="css/fin3.css" />
<style type="text/css">
</style>
	<script type="text/javascript">
		{literal}

		function func1(){
			setTimeout(function () {document.getElementById('msg').style.display='none'}, 2000);
		}
		window.onload = func1;
		
		{/literal}
    </script>
{/block}
{block name=body}	
<form name="agentForm" id="agentForm" method="post">
  <center>
  <table align="center" cellpadding="0" cellspacing="0">		
    
    <h1 align="center">Agent Entry</h1>
	<h3 id="msg" align="center" style="color:red;">{$msg}</h3>
    <tr>
    	<th align="left">Agent Name :</th>
    	<td><input type="text" required=required  name="agent_name" autofocus id="agent_name" placeholder="Agent Name" value="{$agentArrEd['agentNameEd']}"></td>
    </tr>
    <tr>
    	<th align="left">Phone1 :</th>
    	<td><input type="text" name="phone1" placeholder="Phone No" id="phone1" value="{$agentArrEd['agentPhoneEd']}"></td>
    </tr>
    <tr>
    	<th align="left">Commission</th>
    	<td><input type="text" name="commission" placeholder="Commission" id="commission" value="{$agentArrEd['agentCommissionEd']}"></td>
    </tr>
    <tr>
      <td colspan="2" align="center">
		<input type="hidden" name="edit" id="edit" value="{$agentArrEd['agentIdEd']}">
        <input type="reset" name="reset" id="reset" value="Reset">
        <input type="submit" name="sub" id="sub" value="SAVE">
      </td>       
    </tr>
  </table>
  </br>
  </br>
  <table align="center" cellpadding="2" cellspacing="2" width="600px">		
    
    <h1 align="center">Agent List</h1>
    <tr>
    	<th align="left" colspan="2">Action</th>
    	<th align="left">Agent Name : </th>
    	<th align="left">Phone No : </th>
    	<th align="left">Commission </th>
    </tr>
	{section name=sec loop=$agentArr}
    <tr>
    	<td align="left"><a href="agentEntry.php?id={$agentArr[sec].agentId}&op=edit">Edit</a></td>
		<td align="left"><a href="agentEntry.php?id={$agentArr[sec].agentId}" onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
		<td align="left">{$agentArr[sec].agentName}</td>
		<td align="left">{$agentArr[sec].phoneNo}</td>
		<td align="left">{$agentArr[sec].commission}</td>
    </tr>
	{/section}
  </table>
  
  
  
</center>
</form>
{/block}