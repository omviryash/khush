{extends file="./link.tpl"}
{block name="head"}
  <script src="js/jquery.min.js" ></script>
  <script type="text/javascript">
  </script>
{/block}

{block name=body}
<form name="loanSummary" method="get">
  <table align="center" cellpadding="5" cellspacing="5" border="1">
  	<h1 align="center">Loan Summary Agent Wise</h1>
  	<tr>
  		<td>Agent :
  		  <select name="agentId" id="agentId" onchange="this.form.submit();">
          <option value="0">All Agent</option>
          {html_options values=$agentArr.agentId  output=$agentArr.agentName selected=$selectedAgent}
        </select>
  		</td>
  		<td>Till Date :
  			{html_select_date prefix='curDate' field_order="DmY" month_format="%m" start_year='-5' end_year='+5' time=$curDate}&nbsp;&nbsp;&nbsp;&nbsp;
  	  </td>
  	  <td>
  	    <input type="submit" value="GO...!" />
  	  </td>
  	</tr>
  </table>
  <center>
    <table border="1" cellspacing="0" cellpadding="3">
      <tr>
        <th align="center">Loan Type</th>
        <th align="center">Party Name</th>
        <th align="center">Loan No</th>
        <th align="center">Loan Date</th>
        <th align="center">Loan Amount</th>
        <th align="right">Interest Received</th>
        <th align="center">Received Amount</th>
        <th align="center">Pending Amount</th>
      </tr>
      {section name=sec loop=$loanArray}
      <tr>
        <td align="center">{$loanArray[sec]['loanType']}</td>
        <td align="center">
        	{$loanArray[sec]['partyName']} 
        </td>
        <td align="center">{$loanArray[sec]['loanId']}</td>
        <td align="center">{$loanArray[sec]['loanDate']|date_format:"%d-%m-%Y"}</td>
        <td align="right">
        	{$loanArray[sec]['loanAmount']}
        	{assign var="loanAmountTotal" value="`$loanAmountTotal+$loanArray[sec]['loanAmount']`"}
        </td>
        <td align="right">
        	{$loanArray[sec]['interestReceived']}
        	{assign var="interestReceivedTotal" value="`$interestReceivedTotal+$loanArray[sec]['interestReceived']`"}
        </td>
        <td align="right">
        	{$loanArray[sec]['receivedAmount']}
        	{assign var="receivedAmountTotal" value="`$receivedAmountTotal+$loanArray[sec]['receivedAmount']`"}
        </td>
        <td align="right">
        	{$loanArray[sec]['pendingAmount']}
        	{assign var="pendingAmountTotal" value="`$pendingAmountTotal+$loanArray[sec]['pendingAmount']`"}
        </td>
      </tr>
      {/section}
      <tr>
      	<th colspan="4" align="right">Total : </th>
      	<th align="right">{$loanAmountTotal}</th>
      	<th align="right">{$interestReceivedTotal}</th>
      	<th align="right">{$receivedAmountTotal}</th>
      	<th align="right">{$pendingAmountTotal}</th>
      </tr>
    </table>
  </center>
</form>
{/block}
