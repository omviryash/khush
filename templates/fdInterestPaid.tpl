{extends file="./link.tpl"}
{block name="head"}
  <script src="js/jquery.min.js" ></script>
  <script type="text/javascript">
  </script>
{/block}

{block name=body}
<form name="fdClientList" method="get">
  <table align="center" cellpadding="5" cellspacing="5" border="1">
  	<h1 align="center">Interest List</h1>
  	<!--<tr>
  		<td>Party :
			<select name="partyId" id="partyId" onchange="this.form.submit();">
			  <option value="0">All Party</option>
			  {html_options values=$arrParty.partyId  output=$arrParty.partyName selected=$selectedParty}
			</select>
  		</td>
  	</tr>-->
  </table>
  <center>
    <table border="1" cellspacing="0" cellpadding="3">
      <tr>
        <th align="center">FD ID</th>
        <th align="center">Party Name</th>
        <th align="center">Lidha</th>
		<th align="center">Aapya</th>
		<th align="center">Interest Date</th>
      </tr>
      {section name=sec loop=$loanArray}
      <tr>
        <td align="center">{$loanArray[sec]['fd_id']}</td>
        <td align="center">{$loanArray[sec]['partyName']}</td>
        {if $loanArray[sec]['lidha_aapya'] == 'lidha'}
			<td align="center">{$loanArray[sec]['int_amount']}</td>
		{else}
			<td align="center">&nbsp;</td>
		{/if}
		{if $loanArray[sec]['lidha_aapya'] == 'aapya'}
			<td align="center">{$loanArray[sec]['int_amount']}</td>
		{else}
			<td align="center">&nbsp;</td>
		{/if}
		<td align="center">{$loanArray[sec]['int_date']|date_format:"%d-%m-%Y"}</td>
      </tr>
      {/section}
    </table>
  </center>
</form>
{/block}
