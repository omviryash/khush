{extends file="./link.tpl"}
{block name="head"}
  <script src="js/jquery.min.js" ></script>
  <script type="text/javascript">
  </script>
{/block}

{block name=body}
<form name="profitlossSummary" method="post">
  <table align="center">
  	<h1 align="center">Profit/Loss Statement</h1>
  	<tr>
    	<td>Date :
  			{html_select_date prefix='curFromDate' field_order="DmY" month_format="%m" start_year='-5' end_year='+5' time=$curFromDate}
  		</td>
  		
        <td>
  	    	<input type="submit" value="Search" />
  	    </td>
  	</tr>
  </table>
	
  <center style="padding-top:20px;">
    <table border="1" cellspacing="0" cellpadding="3">
      <tr>
        <th align="center">Interest Received</th>
        <th align="center">Interest Paid</th>
        <th align="center">Expenses</th>
		<th align="center">FD Interest Paid</th>
        <th align="center">Agent Commission</th>
        <th align="center">Profit Loss</th>
      </tr>
      <tr>
        <th align="center">{$data_credit[2]}</th>
        <th align="center">{$data_debit[2]}</th>
        <th align="center">{$data_debit[3]}</th>
		<th align="center">{$data_debit[6]}</th>
        <th align="center">{$data_debit[4]}</th>
		<th align="center">{$data_credit[2] - ($data_debit[2] + $data_debit[3] + $data_debit[4] + $data_debit[6])}</th>
      </tr>
    </table>
  </center>
</form>
{/block}
