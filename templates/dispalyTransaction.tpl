{extends file="./link.tpl"}
{block name="head"}
<style type="text/css">
  	input[type="submit"], input[type="reset"] {
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;  
    border-radius: 10px;
    }
    input[type="submit"], input[type="reset"] {
    float: right;
      margin: 2em 1em 0 1em;
      width: 5em;
      padding: .5em;
      border: 1px solid #666;
      -moz-border-radius: 10px;
      -webkit-border-radius: 10px;  
      border-radius: 10px;
      -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      color: #fff;
      background: #0a0;
      font-size: 1em;
      line-height: 1em;
      font-weight: bold;
      opacity: .7;
      -webkit-appearance: none;
      -moz-transition: opacity .5s;
      -webkit-transition: opacity .5s;
      -o-transition: opacity .5s;
      transition: opacity .5s;
    }
    input[type="submit"]:hover,
    input[type="submit"]:active,
    input[type="reset"]:hover,
    input[type="reset"]:active {
      cursor: pointer;
      opacity: 1;
    }
    input[type="submit"]:active, input[type="button"]:active {
      color: #333;
      background: #eee;
      -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
      -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
      box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
    }
    input[type="reset"] {
      background: #f33;
    }	
  </style>  
{/block}
{block name=body}
<br><br><br><br>
<form name="frm" id="frm" method="POST">
<table align="center">
  <tr>
    <td>
    	<select name="party" id="party">
    		<option value="">All</option>
    		{html_options values=$partyArr.partyId output=$partyArr.partyName selected=$partySelect}
      </select>
      <select name="accountId1" id="accountId1">
        {html_options  values=$accountId1Values output=$accountName1 selected=$account1Select}
      </select>
      <select name="accountId2" id="accountId2">
        {html_options  values=$accountId2Values output=$accountName2 selected=$account2Select}
      </select>
       To:
      {html_select_date prefix='startDate' id="startDate" time=$startDateSelect start_year='-5' end_year='+5' field_order="DMY" month_format="%m" day_value_format="%02d"}
       From:
      {html_select_date prefix='endDate' time=$endDateSelect start_year='-5' end_year='+5' field_order="DMY" month_format="%m" day_value_format="%02d"}
    </td>
    <td><input type="submit" name="go" id="go" value="Go"></td>
  </tr>
</table>
</form>
<br><br><br><br>
<table align="center" cellspacing="1" cellpadding="1">
	<tr>
		<td valign="top">
	    <table align="left" border="1">
	    	<tr>
	    		<td>
	    			<h3>Credit</h3>
	    		</td>
	      </tr>
	       {section name=sec1 loop=$creditArray} 
	      <tr>
	         <td>{$creditArray[sec1].desc} : {$creditArray[sec1].date} : {$creditArray[sec1].amount}</td>
	      </tr>
	      {/section}
	      <tr>
	      	<td>Credit Total: {$creditTotal}</td>
	      </tr>
	    </table>
    </td>
    <td valign="top">
			<table align="right" border="1">
			  <tr>
	      <td><h3>DEBIT</h3></td>
	      </tr>
	    {section name=sec loop=$debitArray} 
	      <tr>
	        <td>{$debitArray[sec].desc} : {$debitArray[sec].date} : {$debitArray[sec].amount}</td>
	      </tr>
	    {/section}
	     <tr>
	      	<td>Debit Total: {$debitTotal}</td>
	      </tr>
	    </table>
		</td>
  </tr>
</table>
<table align="center" cellspacing="1" cellpadding="1">
<tr>
	<td>Grand Total:  {$grandTotal}</td>
</tr>
</table>
{/block}