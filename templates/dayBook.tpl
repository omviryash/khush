{extends file="./link.tpl"}
{block name="head"}
  <script src="js/jquery.min.js" ></script>
  <script type="text/javascript">
  </script>
  <script>
  var default_val ='';
  $(document).ready(function(){
	default_val = $('.search_to').html();
	$('.search_by').change(function(){
		if($(this).val()>0){
		 $.ajax({
			 type: 'post',
			 url: 'dayBook.php',
			 data: {
			   ajax:$(this).val()
			 },
			 success: function (response) {
				$('.search_to').html(response);
				$('.search_to').find('option [value = {$SEARCH_TO_SELECT}]');
			 }
		   });
		}else{
			$('.search_to').html(default_val);
		}
	});
	if($('.search_by').val() > 0){
		$.ajax({
				type: 'post',
				url: 'dayBook.php',
				data: {
				   ajax:$('.search_by').val()
				},
				success: function (response) {
					$('.search_to').html(response);
					$('.search_to').find('option[value={$SEARCH_TO_SELECT}]').attr('selected','selected');
				}
			});
	}
  });
  </script>
{/block}

{block name=body}
<form name="dayBook" method="get">
  <table align="center" cellpadding="5" cellspacing="5" border="1">
  	<h1 align="center">Day Book</h1>
		<td>Search By :
  			{html_options name=search_by class="search_by" selected=$SEARCH_BY_SELECT options=$SEARCH_BY }
		</td>
		<td colspan="2">Search To :
  			{html_options name=search_to class="search_to" options=$SEARCH_TO selected=$SEARCH_TO_SELECT}
		</td>
  	<tr>
		<td>From Date :
  			{html_select_date prefix='fromDate' field_order="DmY" day_value_format="%02d" month_format="%m" start_year='2011' end_year='+5' time=$fromDate}&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
		<td>To Date :
  			{html_select_date prefix='toDate' field_order="DmY" day_value_format="%02d" month_format="%m" start_year='2011' end_year='+5' time=$toDate}&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
		<td>
			<input type="submit" value="GO...!" name="go" />
		</td>
  	</tr>
  </table>
  </br></br>
  <center>
  <table border="1" cellspacing="0" cellpadding="4">
  {if $SEARCH_BY_SELECT == 0 && $SEARCH_BY_SELECT == 0}
  <tr>
    <td align="left" colspan="2">Previous Balance : {$transactionArr[0]['balance']}</td>
  </tr>
  {/if}
  <tr>
  <td valign="top" align="center">
    <table border="1" cellspacing="0" cellpadding="4" style="color: blue">
      <tr>
        <td colspan="4" align="center">Credit</td>
      </tr>
      <tr>
		<th align="center">Payment</th>
		<th align="center">Ledger</th>
		<th align="center">Date</th>
        <th align="center">Particulars</th>
        <th align="center">Amount</th>
      </tr>
	  {section name=sec loop=$transactionArr}
	  {if $transactionArr[sec]['creditDebit'] == 'Credit'}
	  <tr>
		<td align="Center"><a href="transactionNew.php">Payment</a></td>
		{if $transactionArr[sec]['transactionForId'] == 1 || $transactionArr[sec]['transactionForId'] == 2}
			<td align="Center"><a href="oneLoanDetail.php?loanId={$transactionArr[sec]['loanId']}&partyId={$transactionArr[sec]['partyId']}">Ledger</a></td>
		{else if $transactionArr[sec]['transactionForId'] == 5 || $transactionArr[sec]['transactionForId'] == 6}
			<td align="Center"><a href="fdLoanDetail.php?loanId={$transactionArr[sec]['loanId']}">Ledger</a></td>
		{else if $transactionArr[sec]['transactionForId'] == 4}
			<td align="Center"><a href="agentReportDetail.php?agentId={$transactionArr[sec]['agentId']}">Ledger</a></td>
		{/if}
		
		<td align="center">{$transactionArr[sec]['transactionDate']}</td>
        <td align="left">{$transactionArr[sec]['note']}</td>
			{if $transactionArr[sec]['agentId'] != 0}
				<td align="right"><a href="dayBook.php?search_by=1&search_to={$transactionArr[sec]['agentId']}&fromDateDay={date('d',strtotime($fromDate))}&fromDateMonth={date('m',strtotime($fromDate))}&fromDateYear={date('Y',strtotime($fromDate))}&toDateDay={date('d',strtotime($toDate))}&toDateMonth={date('m',strtotime($toDate))}&toDateYear={date('Y',strtotime($toDate))}">{$transactionArr[sec]['transactionAmount']}</a> </td>
			{else if $transactionArr[sec]['partyId'] != 0}
				<td align="right"><a href="dayBook.php?search_by=2&search_to={$transactionArr[sec]['partyId']}&fromDateDay={date('d',strtotime($fromDate))}&fromDateMonth={date('m',strtotime($fromDate))}&fromDateYear={date('Y',strtotime($fromDate))}&toDateDay={date('d',strtotime($toDate))}&toDateMonth={date('m',strtotime($toDate))}&toDateYear={date('Y',strtotime($toDate))}">{$transactionArr[sec]['transactionAmount']}</a> </td>
			{else}
				<td align="right">{$transactionArr[sec]['transactionAmount']}</td>
			{/if}
	  </tr>
          {/if}
	  {/section}
      <tr>
        <td align="right" colspan="4">Credit Total</td><td align="right">{$credit_total}</td>
      </tr>
    </table>
  </td>
  <td valign="top" align="center">
    <table border="1" cellspacing="0" cellpadding="4" style="color: red">
      <tr>
        <td colspan="4" align="center">Debit</td>
      </tr>
      <tr>
		<th align="center">Payment</th>
		<th align="center">Ledger</th>
        <th align="center">Date</th>
        <th align="center">Particulars</th>
        <th align="center">Amount</th>
      </tr>
	  {section name=sec loop=$transactionArr}
	  {if $transactionArr[sec]['creditDebit'] == 'Debit'}
	  <tr>
		<td align="Center"><a href="transactionNew.php">Payment</a></td>
		{if $transactionArr[sec]['transactionForId'] == 1 || $transactionArr[sec]['transactionForId'] == 2}
			<td align="Center"><a href="oneLoanDetail.php?loanId={$transactionArr[sec]['loanId']}&partyId={$transactionArr[sec]['partyId']}">Ledger</a></td>
		{else if $transactionArr[sec]['transactionForId'] == 5 || $transactionArr[sec]['transactionForId'] == 6}
			<td align="Center"><a href="fdLoanDetail.php?loanId={$transactionArr[sec]['loanId']}">Ledger</a></td>
		{else if $transactionArr[sec]['transactionForId'] == 4}
			<td align="Center"><a href="agentReportDetail.php?agentId={$transactionArr[sec]['agentId']}">Ledger</a></td>
		{/if}
		
		
        <td align="center">{$transactionArr[sec]['transactionDate']}</td>
        <td align="left">{$transactionArr[sec]['note']}</td>
        {if $transactionArr[sec]['agentId'] != 0}
			<td align="right"><a href="dayBook.php?search_by=1&search_to={$transactionArr[sec]['agentId']}&fromDateDay={date('d',strtotime($fromDate))}&fromDateMonth={date('m',strtotime($fromDate))}&fromDateYear={date('Y',strtotime($fromDate))}&toDateDay={date('d',strtotime($toDate))}&toDateMonth={date('m',strtotime($toDate))}&toDateYear={date('Y',strtotime($toDate))}">{$transactionArr[sec]['transactionAmount']}</a> </td>
		{else if $transactionArr[sec]['partyId'] != 0}
			<td align="right"><a href="dayBook.php?search_by=2&search_to={$transactionArr[sec]['partyId']}&fromDateDay={date('d',strtotime($fromDate))}&fromDateMonth={date('m',strtotime($fromDate))}&fromDateYear={date('Y',strtotime($fromDate))}&toDateDay={date('d',strtotime($toDate))}&toDateMonth={date('m',strtotime($toDate))}&toDateYear={date('Y',strtotime($toDate))}">{$transactionArr[sec]['transactionAmount']}</a> </td>
		{else}
			<td align="right">{$transactionArr[sec]['transactionAmount']}</td>
		{/if}
      </tr>
	  {/if}
	  {/section}
      <tr>
        <td align="right" colspan="4">Debit Total</td><td align="right">{$debit_total}</td>
      </tr>
    </table>
  </td>
  </tr>
  {if $SEARCH_BY_SELECT == 0 && $SEARCH_BY_SELECT == 0}
  <tr>
    <td align="left" colspan="2">Closing Balance : {$balance} </td>
  </tr>
  {/if}
  </table>
  </center>
  
</form>
{/block}
