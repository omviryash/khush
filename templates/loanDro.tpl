{extends file="./link.tpl"}
{block name=head}
  <style type="text/css">
  	input[type="submit"], input[type="reset"] {
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;  
    border-radius: 10px;
    }

    input[type="submit"], input[type="reset"] {
    
    
    float: right;
      margin: 2em 1em 0 1em;
      width: 10em;
      padding: .5em;
      border: 1px solid #666;
      -moz-border-radius: 10px;
      -webkit-border-radius: 10px;  
      border-radius: 10px;
      -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      color: #fff;
      background: #0a0;
      font-size: 1em;
      line-height: 1em;
      font-weight: bold;
      opacity: .7;
      -webkit-appearance: none;
      -moz-transition: opacity .5s;
      -webkit-transition: opacity .5s;
      -o-transition: opacity .5s;
      transition: opacity .5s;
    }
    
    input[type="submit"]:hover,
    input[type="submit"]:active,
    input[type="reset"]:hover,
    input[type="reset"]:active {
      cursor: pointer;
      opacity: 1;
    }
    
    input[type="submit"]:active, input[type="button"]:active {
      color: #333;
      background: #eee;
      -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
      -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
      box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
    }
    
    input[type="reset"] {
      background: #f33;
    }	
  </style>
  <script src="js/jquery.min.js" ></script>
  <script type="text/javascript">
    $(document).ready(function(){
      var dataString = "partyId=" + $("#partyNameSel").val();
	    $.ajax(
	    {
	      type: "GET",
	      url: "enterNewPartyAjax.php",
	      data: dataString,
	      success:function(data)
	      {
	        $('.partyData').html(data);
	      }
	    });
    });
    
  	var garanter;
    function countDays()
    {
    	var loanAmount        = $("#loanAmount").val();
    	var installmentAmount = $("#installmentAmount").val();
    	if(installmentAmount != "" && installmentAmount != 0)
      {
    	  var installmentDays   = loanAmount/installmentAmount;
    	  $("#installmentDays").val(installmentDays);
    	}
    }
    function findGuaranterId(partyObj)
    {
      var dataString = "partyId=" + $("#partyNameSel").val();
	    $.ajax(
	    {
	      type: "GET",
	      url: "enterNewPartyAjax.php",
	      data: dataString,
	      success:function(data)
	      {
	        $('.partyData').html(data);
	      }
	    });
	    
      $.ajax({
        url:'findGuaranterIdAjax.php',
        data:'fid='+partyObj.value,
        type:'post',
        dataType:'html',
        success:function(msg)
        {
          $('#loanGuaranterSel').val(msg);
        }
      });
    }
  </script>		
{/block}
{block name=body}
<form name="loanForm" id="loanForm" action="loan.php" method="post">
  <table cellpadding="2" cellspacing="0" border="0">
  	<caption>
  		<font style="color:red"><b>Last Dro Loan Id :-> {$loanIdFind}</b></font>
  	</caption>
    <tr>
      <td align="right" valign="top" >
  	    <table cellpadding="2" cellspacing="0" border="1">
  	      <tr>
  	        <td colspan="2">
  	        	<h1>Loan Dro Entry</h1>
  	        	<span style="color:red">{$msg}</span>
  	        </td>
  	      </tr>
  	      <tr>
  	      	<th align="right">Loan Id</th>
  	      	<td><input type="text"  name="loanId"  id="loanId"></td>
  	      </tr>
  	      <tr>
            <th align="right">Party : </th>
            <td>
              <select name="partyNameSel" id="partyNameSel" onchange="findGuaranterId(this);">
                <option selected=selected value="0">-- New Party --</option>
                {html_options values=$arrParty.partyId  output=$arrParty.partyName }
              </select>
            </td>
          </tr>
          <tr>
            <th align="right">Date : </th>
            <td>{html_select_date prefix='curDate' field_order="DmY" month_format="%m" start_year='-5' end_year='+5' time=$today}</td>
          </tr>
          <tr>
            <th align="right">Amount : </th>
            <td><input type="text" required=required name="loadAmount" id="loanAmount" onblur="countDays();"></td>
          </tr>
          <tr>
            <th align="right">Interest : </th>
            <td><input type="text" name="loanInterest" id="loanInterest"></td>
          </tr>
          <tr>
            <th align="right">Installment Amount : </th>
            <td><input type="text" name="installmentAmount" required=required id="installmentAmount" onblur="countDays();"></td>
          </tr>
          <tr>
            <th align="right">Installment Days : </th>
            <td><input type="text" name="installmentDays" readonly=readonly id="installmentDays"> </<td>
          </tr>
          <tr>
            <th align="right">Guaranter : </th>
            <td>
              <select name="loanGuaranterSel" id="loanGuaranterSel">
                <option value="">--Guaranter--</option>
                {html_options values=$arrParty.partyId  output=$arrParty.partyName}
              </select>
              <br />
              <input type="text" name="loanGuaranter" id="loanGuaranter">
            </td>
          </tr>
          <tr>
            <th align="right">Installment Received : </th>
            <td><input type="text" name="installmentReceived"></<td>
          </tr>
        </table> 
      </td>
      <td align="left" valign="top" class="partyData">
      </td>
    </tr>
    <tr>
      <td align="center" colspan="2">
        <input type="reset" name="reset" id="reset" value="Reset">
        <input type="submit" name="loanBtn" id="loanBtn" value="Save">
      </td>
    </tr>
  </table>
</form> 
{/block}