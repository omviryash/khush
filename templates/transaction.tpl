{extends file="./link.tpl"}
{block name=head}
<style type="text/css">
  	input[type="submit"], input[type="reset"] {
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;  
    border-radius: 10px;
    }

    input[type="submit"], input[type="reset"] {
    
    
    float: right;
      margin: 2em 1em 0 1em;
      width: 8em;
      padding: .5em;
      border: 1px solid #666;
      -moz-border-radius: 10px;
      -webkit-border-radius: 10px;  
      border-radius: 10px;
      -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      color: #fff;
      background: #0a0;
      font-size: 1em;
      line-height: 1em;
      font-weight: bold;
      opacity: .7;
      -webkit-appearance: none;
      -moz-transition: opacity .5s;
      -webkit-transition: opacity .5s;
      -o-transition: opacity .5s;
      transition: opacity .5s;
    }
    
    input[type="submit"]:hover,
    input[type="submit"]:active,
    input[type="reset"]:hover,
    input[type="reset"]:active {
      cursor: pointer;
      opacity: 1;
    }
    
    input[type="submit"]:active, input[type="button"]:active {
      color: #333;
      background: #eee;
      -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
      -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
      box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
    }
    
    input[type="reset"] {
      background: #f33;
    }	
  </style>  
  <script src="js/jquery-ui.js"></script>
  <script type="text/javascript">
    function InstallmentAmount(obj)
    {
    	if(obj.checked == true)
    	{
    	  var ckval=obj.value;
    	  var chid=obj.id;
    	  $("#Receive"+chid).val(ckval);	
    	}
    	else
    	{
    		var chid=obj.id;
    	  $("#Receive"+chid).val("");
    	}
    }
  </script>
{/block}
{block name=body}	
<form name="accountForm" id="accountForm" method="post">
  <h1 align="center">Account Entry</h1>
  <center>
    <table> 		
      <tr>
        <th align="right">From :</th>
        <td>
          <select name="accountFrom" id="" required>
            <option value="">-- Account From --</option>
            {html_options values=$AccArray.accountId output=$AccArray.accountName}
          </select>
        </td>
        <th align="right">To :</th>
        <td>
          <select name="accountTo" id="" required>
            <option value="">-- Account To --</option>
            {html_options values=$AccArray.accountId output=$AccArray.accountName}
          </select>
        </td>
      </tr>
      <tr>
        <th align="right">Date :</th>
        <td>{html_select_date prefix="transDate" start_year ="-0" end_year="+2" field_order="DMY" month_format="%m" day_value_format="%02d"}</td>
      </tr>
      <tr>
        <th align="right">Amount :</th>
        <td><input type="text" required=required  name="amount" autofocus id="accountName" placeholder="Amount"> 
        </td>
      </tr>
      <tr>
      </tr>
      <tr>
        <th align="right" valign="center">Interst For 30 Days :</th>
        <td>
        	<input type="text" value="0" name="interest30days">
        </td>
      <tr>
        <th align="right" valign="center">Narration :</th>
        <td>
          <textarea cols="17" id="narration" name="narration" rows="5"></textarea>  	
        </td>
      </tr>
      <tr>
        <td colspan="2" align="center">
          <input type="submit" name="Submit" id="sub" value="SAVE" >
        </td>       
      </tr>
    </table>
  </center>
</form>
{/block}