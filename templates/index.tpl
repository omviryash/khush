{extends file="./link.tpl"}
{block name="head"}
<link rel="stylesheet" href="css/fin3.css" />
<style type="text/css">
</style>
{/block}
{block name=body}	
<form name="partyForm" id="partyForm" method="post">
  <center>
  <table align="center" cellpadding="0" cellspacing="0">		
    
    <h1 align="center">Party Entry</h1>
    <tr>
    	<th align="left">Party Name :</th>
    	<td><input type="text" required=required  name="partyName" autofocus id="partyName" placeholder="Party Name"></td>
    </tr>
    <tr>
    	<th align="left">Address</th>
    	<td><textarea cols="17" id="address" name="address" placeholder="Address" rows="5"></textarea></td>
    </tr>
    <tr>
    	<th align="left">City :</th>
    	<td><input type="text" name="city" placeholder="City" id="city"></td>
    </tr>
    <tr>
    	<th align="left">Phone1 :</th>
    	<td><input type="text" name="phone1" placeholder="Phone No" id="phone1"></td>
    </tr>
    <tr>
    	<th align="left">Phone2 :</th>
    	<td><input type="text" name="phone2" placeholder="Phone No" id="phone2"></td>
    </tr>
    <tr>
    	<th align="left">Documents :</th>
    	<td>
            {html_checkboxes name='documents' options=$arrDocuments separator=' '}
    	</td>
    </tr>
    <tr>
      <th align="left">Other Information :</th>
      <td>
        <textarea cols="17" id="otherInfo" name="otherInfo" placeholder="Other Information" rows="5"></textarea>  	
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <input type="reset" name="reset" id="reset" value="Reset">
        <input type="submit" name="sub" id="sub" value="SAVE">
      </td>       
    </tr>
  </table>
</center>
</form>
{/block}