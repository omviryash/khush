<table id="datatables" class="display">
  <thead>
    <tr>
    	<th>Delete</th>
    	<th>Loan Id</th>
      <th>partyName</th>
      <th>loanDate</th>
      <th>loanAmount</th>
      <th>interest</th>
      <th>installmentAmount</th>
      <th>installmentDays</th>
    </tr>
  </thead>
  <tbody>
    {section name=rec loop=$loans} 
      <tr>
      	<td><a href="loansDelete.php?loanId={$loans[rec].loanId}">Delete</a></td>
      	<td>{$loans[rec].loanId}</td>
        <td>{$loans[rec].partyName}</td>
        <td align="center">{$loans[rec].loanDate}</td>
        <td align="right">{$loans[rec].loanAmount}</td>
        <td align="right">{$loans[rec].interest}</td>
        <td align="right">{$loans[rec].installmentAmount}</td>
        <td align="right">{$loans[rec].installmentDays}</td>
      </tr>
    {/section}
  </tbody>
</table>