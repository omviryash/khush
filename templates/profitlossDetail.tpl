{block name="head"}
{/block}
{block name=body}	

  <center>
  <table border="1" cellspacing="0" cellpadding="3">		
    
    <h1 align="center">{$title}</h1>
	{if $type eq 'income'}
	<tr>
        <th align="center">Party Name</th>
        <th align="center">Loan Amount</th>
        <th align="center">Interest</th>
        <th align="center">Date</th>
      </tr>
      {section name=sec loop=$arrIncome}
      <tr>
        <td align="center">{$arrIncome[sec]['partyName']}</td>
        <td align="right">{$arrIncome[sec]['loanAmount']}</td>
        <td align="right">{$arrIncome[sec]['interest']}</td>
        <td align="center">{$arrIncome[sec]['loanDate']|date_format:"%d-%m-%Y"}</td>
      </tr>
      {/section}
	{/if}
	{if $type eq 'expense'}
	<tr>
        <th align="center">Expense Detail</th>
        <th align="center">Expense Amount</th>
        <th align="center">Date</th>
      </tr>
      {section name=sec loop=$arrIncome}
      <tr>
        <td align="center">{$arrIncome[sec]['expence_detail']}</td>
        <td align="right">{$arrIncome[sec]['expence_amount']}</td>
        <td align="center">{$arrIncome[sec]['expence_date']|date_format:"%d-%m-%Y"}</td>
      </tr>
      {/section}
	{/if}
  </table>
</center>
<center>
  <table border="1" cellspacing="0" cellpadding="3">		
    
    <h1 align="center">{$subtitle}</h1>
	<tr>
        <th align="center">Party Name</th>
        <th align="center">Loan Amount</th>
        <th align="center">Interest</th>
        <th align="center">Date</th>
      </tr>
      {section name=sec loop=$arrInterest}
      <tr>
        <td align="center">{$arrInterest[sec]['partyName']}</td>
        <td align="right">{$arrInterest[sec]['loanAmount']}</td>
        <td align="right">{$arrInterest[sec]['interest']}</td>
        <td align="center">{$arrInterest[sec]['loanDate']|date_format:"%d-%m-%Y"}</td>
      </tr>
      {/section}
  </table>
</center>

{/block}