{extends file="./link.tpl"}
{block name="head"}
  <script src="js/jquery.min.js" ></script>
  <script type="text/javascript">
  </script>
{/block}

{block name=body}
<form name="loanSummary" method="get">
  <table align="center" cellpadding="5" cellspacing="5" border="1">
  	<h1 align="center">Loan Pending Installment To Current Date Party Wise</h1>
  	<tr>
		<td> From Date : {html_select_date prefix='fromDate' field_order="DmY" month_format="%m" start_year='-2' end_year='+1' time=$fromDate}</td>
		<td> To Date{html_select_date prefix='toDate' field_order="DmY" month_format="%m" start_year='-2' end_year='+1' time=$toDate}</td>
		<td>Party :
  		  <select name="partyId" id="partyId">
		  <option value="0">All Party</option>
          {html_options values=$partyArr.partyId  output=$partyArr.partyName selected=$selectedParty}
			</select>
  		</td>
		<td>
			<input type="submit" value="GO...!" />
		</td>
  	</tr>
  </table>
  <center>
    <table border="1" cellspacing="0" cellpadding="3">
      <tr>
        <th align="center">Loan Type</th>
        <th align="center">Party Name</th>
        <th align="center">Loan No</th>
        <th align="center">Loan Date</th>
        <th align="center">Loan Amount</th>
        <th align="center">Pending Loan Amount</th>
        <th align="center">Pending Interest</th>
        <th align="center">Pending Interest Months</th>
		<th align="center">Loan End Date</th>
      </tr>
      {section name=sec loop=$loanArray}
      <tr>
        <td align="center">{$loanArray[sec]['loanType']}</td>
        <td align="center">
        	{if $selectedParty > 0} 
        	  {$onePartyName}
        	{else} 
            {$loanArray[sec]['partyName']} 
        	{/if}
        </td>
        <td align="center">{$loanArray[sec]['loanId']}</td>
        <td align="center">{$loanArray[sec]['loanDate']|date_format:"%d-%m-%Y"}</td>
        <td align="right">
        	{$loanArray[sec]['loanAmount']}
        	{assign var="loanAmountTotal" value="`$loanAmountTotal+$loanArray[sec]['loanAmount']`"}
        </td>
		<td align="right">
        	{$loanArray[sec]['pendingLoanAmount']}
        </td>
		<td align="right">
        	&nbsp;
        </td>
		<td align="center">{$loanArray[sec]['pendingInstallment']}</td>
        <td align="center">{$loanArray[sec]['loanEndDate']|date_format:"%d-%m-%Y"}</td>
      </tr>
      {/section}
	  {section name=sec loop=$loanArray1}
      <tr>
        <td align="center">{$loanArray1[sec]['loanType']}</td>
        <td align="center">
        	{if $selectedParty > 0} 
        	  {$onePartyName}
        	{else} 
            {$loanArray1[sec]['partyName']} 
        	{/if}
        </td>
        <td align="center">{$loanArray1[sec]['fd_id']}</td>
        <td align="center">{$loanArray1[sec]['fd_date']|date_format:"%d-%m-%Y"}</td>
        <td align="right">
        	{$loanArray1[sec]['fd_amount']}
        	{assign var="loanAmountTotal1" value="`$loanAmountTotal1+$loanArray1[sec]['fd_amount']`"}
        </td>
		<td align="center">{$loanArray1[sec]['pendingLoanAmount']}</td>
		<!--<td align="center">&nbsp;</td>-->
		<td align="center">{$loanArray1[sec]['pendingInterest']}</td>
		<td align="center">{$loanArray1[sec]['pendingInstallment']}</td>
		<td align="center">&nbsp;</td>
      </tr>
      {/section}
    </table>
  </center>
</form>
{/block}
