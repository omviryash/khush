{extends file="./link.tpl"}
{block name="head"}
  <script src="js/jquery.min.js" ></script>
  <script type="text/javascript">
  
  </script>
{/block}

{block name=body}
<form name="loanSummary" method="get">
  <table align="center" cellpadding="5" cellspacing="5" border="1">
  	<h1 align="center">Loan Summary</h1>
  	<tr>
  		<td>Party :
  		  <select name="partyId" id="partyId" onchange="this.form.submit();">
          <option value="0">All Party</option>
          {html_options values=$partyArr.partyId  output=$partyArr.partyName selected=$selectedParty}
        </select>
  		</td>
  		<td>Agent :
  		  <select name="agentId" id="agentId" onchange="this.form.submit();">
          <option value="0">All Agent</option>
          {html_options values=$agentArr.agentId  output=$agentArr.agentName selected=$selectedAgent}
        </select>
  		</td>
  		<td>Gaurantor :
  		  <select name="gaurantorId" id="gaurantorId" onchange="this.form.submit();">
          {html_options values=$gaurantorArr.partyId  output=$gaurantorArr.partyName selected=$selectedGaurantor}
        </select>
  		</td>
		<td>Till Date :
  			{html_select_date prefix='curDate' field_order="DmY" month_format="%m" start_year='-5' end_year='+5' time=$curDate}&nbsp;&nbsp;&nbsp;&nbsp;
  	  </td>
  	  <td>
  	    <input type="submit" value="GO...!" />
  	  </td>
  	</tr>
  </table>
  <center>
    <table border="1" cellspacing="0" cellpadding="4">
      <tr>
		<th align="center">Click For Detail</th>
        <th align="center">Loan Type</th>
        <th align="center">Party Name</th>
        <th align="center">Loan No</th>
        <th align="center">Loan Date</th>
        <th align="center">Loan Amount</th>
        <th align="right">Interest Received</th>
        <th align="center">FD Amount</th>
        <th align="right">Interest Paid</th>
        <th align="center">Returned Amount</th>
        <th align="center">Pending Amount</th>
		<th align="center">Pending Installment</th>
		<th align="center">Pending Interest</th>
		<th align="center">Loan End Date</th>
      </tr>
      {section name=sec loop=$loanArray}
      <tr>
		<td align="center">
		  <a href="oneLoanDetail.php?loanId={$loanArray[sec]['loanId']}&partyId={$loanArray[sec]['partyId']}">Detail</a>
		  <a href="loansDelete.php?loanId={$loanArray[sec]['loanId']}&redirect=loanSummaryNew" onclick="return confirm('Are you sure you want to delete?')" >Delete</a>
		</td>
        <td align="left">{$loanArray[sec]['loanType']}</td>
        <td align="left">{$loanArray[sec]['partyName']}</td>
        <td align="center">{$loanArray[sec]['loanId']}</td>
        <td align="center" NOWRAP>{$loanArray[sec]['loanDate']|date_format:"%d-%m-%Y"}</td>
        <td align="right">
        	{$loanArray[sec]['loanAmount']}
        	{assign var="loanAmountTotal" value="`$loanAmountTotal+$loanArray[sec]['loanAmount']`"}
        </td>
        <td align="right">
        	{$loanArray[sec]['interestReceived']}
        	{assign var="interestReceivedTotal" value="`$interestReceivedTotal+$loanArray[sec]['interestReceived']`"}
        </td>
        <td align="right">&nbsp;</td>
        <td align="right">&nbsp;</td>
        <td align="right">
        	{$loanArray[sec]['receivedAmount']}
        	{assign var="receivedAmountTotal" value="`$receivedAmountTotal+$loanArray[sec]['receivedAmount']`"}
        </td>
		<td align="right">
        	{$loanArray[sec]['pendingAmount']}
        	{assign var="pendingAmountTotal" value="`$pendingAmountTotal+$loanArray[sec]['pendingAmount']`"}
        </td>
		{if $loanArray[sec]['loanType'] == 'Monthly'}
		<td><a href="loanMonthly.php?loanId={$loanArray[sec]['loanId']}">{$loanArray[sec]['pendingInstallment']}</a></td>
		{else}
		<td>{$loanArray[sec]['pendingInstallment']}</td>
		{/if}
		<td align="right">
			{$loanArray[sec]['pendingInterest']}
			{assign var="pendingInterestTotal" value="`$pendingInterestTotal+$loanArray[sec]['pendingInterest']`"}
		</td>
        
		
		
		<td>{$loanArray[sec]['loanEndDate']|date_format:"%d-%m-%Y"}</td>
      </tr>
      {/section}
<!--      <tr>
      	<th colspan="5" align="right">Total : </th>
      	<th align="right">{$loanAmountTotal}</th>
      	<th align="right">{$interestReceivedTotal}</th>
      	<th align="right">{$receivedAmountTotal}</th>
      	<th align="right">{$pendingAmountTotal}</th>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
      </tr>-->
      {section name=sec loop=$loanArray1}
      <tr>
		<td align="center">
		  <a href="fdLoanDetail.php?loanId={$loanArray1[sec]['fd_id']}&partyId={$loanArray1[sec]['partyId']}">Detail</a>
		  <a href="loansDelete.php?fd_id={$loanArray1[sec]['fd_id']}&redirect=loanSummaryNew" onclick="return confirm('Are you sure you want to delete?')" >Delete</a>
		</td>
        <td align="left">{$loanArray1[sec]['loanType']}</td>
        <td align="left">{$loanArray1[sec]['partyName']}</td>
        <td align="center">{$loanArray1[sec]['fd_id']}</td>
        <td align="center" NOWRAP>{$loanArray1[sec]['fd_date']|date_format:"%d-%m-%Y"}</td>
        <td align="right">&nbsp;</td>
        <td align="right">&nbsp;</td>
        <td align="right">
        	{$loanArray1[sec]['fd_amount']}
        	{assign var="fdAmountTotal" value="`$fdAmountTotal+$loanArray1[sec]['fd_amount']`"}
        </td>
        <td align="right">
			{$loanArray1[sec]['interestReceived']}
        	{assign var="interestPaidTotal" value="`$interestPaidTotal+$loanArray1[sec]['interestReceived']`"}
        </td>
        <td align="right">
        	{$loanArray1[sec]['receivedAmount']}
        	{assign var="receivedAmountTotalFD" value="`$receivedAmountTotalFD+$loanArray1[sec]['receivedAmount']`"}
        </td>
        <td align="right">
        	{$loanArray1[sec]['pendingAmount']}
        	{assign var="pendingAmountTotalFD" value="`$pendingAmountTotalFD+$loanArray1[sec]['pendingAmount']`"}
        </td>
		<td>{$loanArray1[sec]['pendingInstallment']}</td>
		<td align="right">
			{$loanArray1[sec]['pendingInterest']}
			{assign var="pendingInterestTotalFD" value="`$pendingInterestTotalFD+$loanArray1[sec]['pendingInterest']`"}
		</td>
		<td NOWRAP>{$loanArray1[sec]['loanEndDate']|date_format:"%d-%m-%Y"}</td>
      </tr>
      {/section}
      <tr>
      	<th colspan="5" align="right">Total : </th>
      	<th align="right">{$loanAmountTotal}</th>
      	<th align="right">{$interestReceivedTotal}</th>
      	<th align="right">{$fdAmountTotal}</th>
      	<th align="right">{$interestPaidTotal}</th>
      	<th align="right">
      	  Loan : {$receivedAmountTotal} </br>
      	  FD   : {$receivedAmountTotalFD} </br>
      	</th>
      	<th align="right">
      	  Loan : {$pendingAmountTotal} </br>
      	  FD : {$pendingAmountTotalFD}
      	</th>
		<td>&nbsp;</td>
		<th align="right">
  		Loan : {$pendingInterestTotal} </br>
  		FD   : {$pendingInterestTotalFD} </br>
		</th>
		<td>&nbsp;</td>
      </tr>
    </table>
  </center>
</form>

{/block}
