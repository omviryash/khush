{extends file="./link.tpl"}
{block name="head"}
<link rel="stylesheet" href="css/fin3.css" />
<style type="text/css">
</style>
{/block}
{block name=body}	
<script src="js/jquery.min.js" ></script>
 <script type="text/javascript">
    function showPartyform()
    {
	  var dataString = $("#amount_party").val();
	  if(dataString=='0'){
	    $.ajax(
	    {
	      type: "GET",
	      url: "enterNewPartyAjax.php",
	      data: "partyId="+dataString,
	      success:function(data)
	      {
			$('#party_form').show();
	        $('.partyData').html(data);
	      }
	    });
	  }
	  else{
		  $('#party_form').hide();
	      $('.partyData').html('');
	  }
    }
 </script>
<form name="expenceForm" id="expenceForm" method="post">
  <center>
  <table align="center" cellpadding="0" cellspacing="0">		
    
    <h1 align="center">FD Amount Entry</h1>
    <tr>
    	<th align="right">FD Party : </th>
    	<td>
        <select name="fd_party_id" id="fd_party_id" onchange="showPartyform();">
            <option value="">-- Select Party --</option>
			{html_options values=$arrParty.partyId  output=$arrParty.partyName}
       <!--     <option value="0">-- New Party --</option>-->
        </select>
        </td>
    </tr>
    <tr id="party_form" style="display:none;">
		<td class="partyData" colspan="2">
		</td>
    </tr>
    <tr>
    	<th align="right">FD Amount : </th>
    	<td><input type="text" name="fd_amount" placeholder="FD Amount" id="fd_amount" required="required"></td>
    </tr>
	<tr>
    	<th align="right">FD Interest Rate : </th>
    	<td><input type="text" name="fd_interest_rate" id="fd_interest_rate" placeholder="FD Interest Rate"></td>
    </tr>
	<tr>
    	<th align="right">Lidha / Aapya : </th>
    	<td>
			<select name="lidha_aapya" id="lidha_aapya">
				<option value="lidha">Lidha</option>
			</select>
        </td>
    </tr>
    <tr>
    	<th align="right">FD Date : </th>
    	<td>{html_select_date prefix='curDate' field_order="DmY" day_value_format="%02d" month_format="%m" start_year='-5' end_year='+5' time=$today}</td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <input type="reset" name="reset" id="reset" value="Reset">
        <input type="submit" name="sub" id="sub" value="SAVE">
      </td>       
    </tr>
  </table>
</center>
</form>
{/block}