{extends file="./link.tpl"}
{block name="head"}
  <script src="js/jquery.min.js" ></script>
  <script type="text/javascript">
    // start set value of loanDetailRowtext box
    function setLoanDetailRow(obj2, loanDetailRow1)
    {
      var loanDetailRow2 = obj2.value;
      var loanDetailRow  = loanDetailRow1 * loanDetailRow2 ;
      $(obj2).next('input:text').val(loanDetailRow);	
//      if((obj2) != "" && (obj2) != 0)
//      {
//        $('.myChk').attr('checked','checked');
//      }
    }
    // end set value of loanDetailRowtext box
    // start set value of installmentReceived text box
    function setInstallReciived(obj1)
    {
      if(obj1.checked == true)
    	{
    	  var loanDetailRow1 = obj1.value;
    	  var chid = obj1.id;
    	  $("#installmentReceived"+chid).val('1');
          $("#installmentReceived"+chid).next('input:text').val(loanDetailRow1);
    	}
    	else
    	{
          var chid=obj1.id;
    	  $("#installmentReceived"+chid).val("");
    	}
    }
	function getDate()
	{
		var y = installmentForm.curDateYear.value;
		var m = installmentForm.curDateMonth.value;
		var d = installmentForm.curDateDay.value;
		var t = y + "-" + m + "-" + d;
		installmentForm1.receivedDate.value = t;
	}
	// end set value of installmentReceived text box
  </script>
{/block}

{block name=body}
<br/>
<form name="installmentForm" method="post">
  <table align="center" cellpadding="5" cellspacing="5" border="1">
  	<h1 align="center">Loan Detail</h1>
  	<tr>
  		<td>
  		<!--select name="viewType" onchange="this.form.sumbit();"> 
  		  {html_options values=$viewTypeValues  output=$viewTypeOutput selected=$viewTypeSelected}
  		</select-->
  		Loan Id:<input type="text" value="{if isset($loanId) && $loanId!= 0}{$loanId}{/if}" name="loanId" id="loanId"></td>
  		<!--td>
  		  <select name="partyNameSel" id="partyNameSel">
          <option selected=selected value="0">-- All --</option>
          {html_options values=$selectPartyId  output=$selectPartyName selected=$selectedParty}
        </select>
  		</td-->
      <td>Party: <select name="partyNameSel" id="partyNameSel" >
                {html_options selected="$partyId" values=$arrParty.partyId  output=$arrParty.partyName}
              </select></td>  
  	  <th>Receive Payment For : </th>
  	  <td>
  	  	{html_select_date prefix='curDate' field_order="DmY" month_format="%m" start_year='-5' end_year='+5' time=$curDate selected=$curDate onchange="getDate();"}&nbsp;&nbsp;&nbsp;&nbsp;
  	  	<input type="submit" name="go" id="go" value="Go">
  	  </td>
  	</tr>
  </table>
</form>
<form name="installmentForm1" method="post">

  <center>
    <table border="1" cellspacing="0" cellpadding="3">
      <tr>
        <th align="center">Last Installment </th>
        <th align="center">Installment <br />Received </th>
        <th align="center">Installment</th>
        <th align="right">Loan No.</th>
        <th align="center">Received</th>
      </tr>
	  {section name=m loop=$loanDetailRow}
      <tr>
        <td align="center">{$loanDetailRow[m]['lastInstallmentDate']|date_format:"%d-%m-%Y"}</td>
        <td align="right">{$loanDetailRow[m]['installmentReceived']}</td>
        <td align="right">{$loanDetailRow[m]['installmentAmount']}</td>
        <td align="right">{$loanDetailRow[m]['partyName']} : {$loanDetailRow[m]['loanId']} : </td>
        <td align="right">
          <input type="checkbox" class="myChk" id="{$loanDetailRow[m]['loanId']}" value="{$loanDetailRow[m]['installmentAmount']}" onclick="setInstallReciived(this);"/>
          <input type="text" size="5" id="installmentReceived{$loanDetailRow[m]['loanId']}" name="installmentReceived[{$loanDetailRow[m]['loanId']}]" 
                 onkeyup="setLoanDetailRow(this,{$loanDetailRow[m]['installmentAmount']});"  value="" />
          <input type="text" size="5" readonly id="amountReceived{$loanDetailRow[m]['loanId']}" name="amountReceived{$loanDetailRow[m]['loanId']}"  />
		  <input type="hidden" name="partyName{$loanDetailRow[m]['loanId']}" value="{$loanDetailRow[m]['partyName']}" />
		  <input type="hidden" name="loanIdS{$loanDetailRow[m]['loanId']}" value="{$loanDetailRow[m]['loanId']}" />
	    </td>
      </tr>
	  {/section}
		<tr>
			<td colspan="5" align="center"><input type="hidden" name="receivedDate" id="receivedDate" ></td>
			<td colspan="5" align="center"><input type="submit" name="sub" id="sub" value="SAVE" onclick="getDate();"></td>
		</tr>
    </table>
  </center>
</form>
{/block}
