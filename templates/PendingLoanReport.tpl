{extends file="./link.tpl"}
{block name=head}
  <style type="text/css">
  	input[type="submit"], input[type="reset"] {
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;  
    border-radius: 10px;
    }

    input[type="submit"], input[type="reset"] {
    
    
    float: right;
      margin: 2em 1em 0 1em;
      width: 10em;
      padding: .5em;
      border: 1px solid #666;
      -moz-border-radius: 10px;
      -webkit-border-radius: 10px;  
      border-radius: 10px;
      -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
      color: #fff;
      background: #0a0;
      font-size: 1em;
      line-height: 1em;
      font-weight: bold;
      opacity: .7;
      -webkit-appearance: none;
      -moz-transition: opacity .5s;
      -webkit-transition: opacity .5s;
      -o-transition: opacity .5s;
      transition: opacity .5s;
    }
    
    input[type="submit"]:hover,
    input[type="submit"]:active,
    input[type="reset"]:hover,
    input[type="reset"]:active {
      cursor: pointer;
      opacity: 1;
    }
    
    input[type="submit"]:active, input[type="button"]:active {
      color: #333;
      background: #eee;
      -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
      -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
      box-shadow: 0 0 .5em rgba(0, 0, 0, .8) inset;
    }
    
    input[type="reset"] {
      background: #f33;
    }
	form{
		padding-top:50px;
	}
	.report-table td , .report-table th{
		padding:5px;
	}
  </style>
  <script src="js/jquery.min.js" ></script>
  	
{/block}
{block name=body}
<form name="loanForm" id="loanForm" method="post">
<center>
	<table width="700" cellspacing="0" cellpadding="2" class="report-table" border="0">
		<tr>
			<td align="center"> 
				{html_select_date prefix='fromDate' field_order="DmY" month_format="%m" start_year='-2' end_year='+1' time=$today}
				<button type="submit" name='search'>Search</button>
			</td>
		</tr>
	</table><br />
	<table width="700" cellspacing="0" cellpadding="2" class="report-table" border="0">
		<tr>
			<td align="center"> 
					{if isset($smarty.post.fromDateDay)}
						Report Date : {$today}
					{else}
						Report Date : {date('d-m-Y',strtotime($today))}
					{/if}
			</td>
		</tr>
	</table><br />
	<table cellpadding="2" class="report-table" width="700" cellspacing="0"  border="1">		
		<tr>
			<th>Name</th>
			<th>Pending Interest</th>
			<th>Pending Loan</th>
			<th>Total Pending</th>
		</tr>
		{if count($selLoanDetailRowData) eq 1}
			<tr>
			<td colspan="4" align="center"> No Data Found </td>
			</tr>
		{else}
		{section name=m loop=$selLoanDetailRowData}
		<tr>
			<td align="center">{$selLoanDetailRowData[m]['partyName']}</td>
			<td align="center">{number_format($selLoanDetailRowData[m]['pending_interest'],2,'.',',')}</td>
			<td align="center">{number_format($selLoanDetailRowData[m]['pending_loan_amount'],2,'.',',')}</td>
			<td align="center">{number_format($selLoanDetailRowData[m]['total_pending'],2,'.',',')}</td>
		</tr>
		{/section}
		{/if}
	</table>
</center>
</form> 
{/block}