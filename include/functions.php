<?php

//function is use to return due date with +1 month
function due_date($due_date){
	$due_date = date('Y-m-d H:m:s',strtotime("+1 month",strtotime($due_date)));
	return $due_date;
}

function date_ymd_to_dmy($dt){
 	 $show_date = substr($dt,8,2)."-".substr($dt,5,2)."-".substr($dt,0,4);
	 return $show_date;
}

?>