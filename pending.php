<?php
include "include/config.inc.php";
$loanArray      = array();
$loanArray1      = array();
$partyArr       = array();
$j              = 0;
$i              = 0;
$k 				= 0;
$curDate        = date('Y-m-d');
$curDate        = isset($_GET['curDateYear']) ? $_GET['curDateYear']."-".$_GET['curDateMonth']."-".$_GET['curDateDay'] : date("Y-m-d"); 

$curFromDate    = isset($_GET['fromDateYear']) ? $_GET['fromDateYear']."-".$_GET['fromDateMonth']."-".$_GET['fromDateDay'] : date("Y-m-d"); 
$curToDate      = isset($_GET['toDateYear']) ? $_GET['toDateYear']."-".$_GET['toDateMonth']."-".$_GET['toDateDay'] : date("Y-m-d"); 

$selectedParty  = isset($_GET['partyId']) ? $_GET['partyId'] : 0;
$getForAllParty = 0; 
$onePartyName   = 0; 
$selPartyQry 	= "SELECT partyId,partyName FROM party ORDER BY partyName";

$selPartyQryRes = mysql_query($selPartyQry);
while($partyRow = mysql_fetch_array($selPartyQryRes))
{
	$partyArr['partyId'][$j]   = $partyRow['partyId'];
	$partyArr['partyName'][$j] = $partyRow['partyName'];
	if(isset($_GET['partyId']) && $_GET['partyId'] > 0 && $_GET['partyId'] == $partyRow['partyId'])
	{
	  $onePartyName   = $partyRow['partyName'];
	}
	
	if(isset($_GET['partyId']) && $_GET['partyId'] > 0)
	{
		$getDataParty      = $_GET['partyId'];
		$selectedPartyName = $partyRow['partyName'];
	}
	else
	{
		$getDataParty      = $partyRow['partyId'];
		$selectedPartyName = $partyRow['partyName'];
	}

	if($getForAllParty == 0)
	{
		if(isset($_GET['partyId']) && $_GET['partyId'] > 0)
		{
			$getForAllParty = 1;
		}
		$selLoanQry = "SELECT loanId,loanDate,loanAmount,interest,installmentAmount,installmentDays,loanTypeId
		                 FROM loan
		                WHERE loan.loanDate BETWEEN '".$curFromDate."' and '".$curToDate."' AND loan.partyId = ".$getDataParty;
		$selLoanQryRes = mysql_query($selLoanQry);
		
		
		if(mysql_num_rows($selLoanQryRes) > 0 ) {
			while($loanRow = mysql_fetch_array($selLoanQryRes))
			{
				if($loanRow['loanTypeId'] == 1)
				{//daily
					$loanArray[$i]['loanId']           	= $loanRow['loanId'];
					$loanArray[$i]['partyName']        	= $selectedPartyName;
					$loanArray[$i]['loanType']         	= 'Daily';
					$loanArray[$i]['loanDate']         	= $loanRow['loanDate'];
					$loanArray[$i]['loanAmount']       	= $loanRow['loanAmount'];	
					$loanArray[$i]['interestReceived'] 	= $loanRow['interest'];
					$date=date_create($loanRow['loanDate']);
					date_add($date,date_interval_create_from_date_string($loanRow['installmentDays']." days"));
					$loanArray[$i]['loanEndDate']	   = date_format($date,"Y-m-d");
					$reciveLoanAmount 					= "select sum(transactionAmount) as transactionAmount from transactionnew where loanId = ".$loanRow['loanId']." AND creditDebit='Credit' AND transactionDate BETWEEN '".$curFromDate."' and '".$curToDate."' group by loanId";
					$rsReciveLoanAmount 				= mysql_query($reciveLoanAmount);
					if(mysql_num_rows($rsReciveLoanAmount) > 0 ) {
						$rsReciveLoanAmount 				 = mysql_fetch_array($rsReciveLoanAmount);
						$loanArray[$i]['pendingLoanAmount'] = $loanArray[$i]['loanAmount'] - $rsReciveLoanAmount['transactionAmount'];
					} else {
						$loanArray[$i]['pendingLoanAmount'] = $loanArray[$i]['loanAmount'];
					}
					$selPendingInstallment = "select * from installment where loanId = ".$loanRow['loanId']." and installmentDate BETWEEN '".$curFromDate."' and '".$curToDate."' ";
					$selPendingInstallmentRes = mysql_query($selPendingInstallment);
					$pi = 0;
					while($selPendingInstallmentResRow = mysql_fetch_array($selPendingInstallmentRes))
					{
						if($selPendingInstallmentResRow['isReceived'] == 'N')
						{
							$pi = $pi + 1;
						}
					}
					
					$loanArray[$i]['pendingInstallment'] = $pi;
				}
				if($loanRow['loanTypeId'] == 4)
				{//monthly
					$loanArray[$i]['loanId']           	= $loanRow['loanId'];
					$loanArray[$i]['partyName']        	= $selectedPartyName;
					$loanArray[$i]['loanType']         	= 'Monthly';
					$loanArray[$i]['loanDate']         	= $loanRow['loanDate'];
					$loanArray[$i]['loanAmount']       	= $loanRow['loanAmount'];	
					$loanArray[$i]['interestReceived'] 	= $loanRow['interest'];
					$loanArray[$i]['loanEndDate']	   	= '';
					
					
					//$loanArray[$i]['pendingInstallment']  = "Not Pending";
					$reciveLoanAmount 					= "select sum(transactionAmount) as transactionAmount from transactionnew where loanId = ".$loanRow['loanId']." AND creditDebit='Credit' AND transactionDate BETWEEN '".$curFromDate."' and '".$curToDate."' group by loanId";
					$rsReciveLoanAmount 				= mysql_query($reciveLoanAmount);
					if(mysql_num_rows($rsReciveLoanAmount) > 0 ) {
						$rsReciveLoanAmount 				 = mysql_fetch_array($rsReciveLoanAmount);
						$loanArray[$i]['pendingLoanAmount'] = $loanArray[$i]['loanAmount'] - $rsReciveLoanAmount['transactionAmount'];
					} else {
						$loanArray[$i]['pendingLoanAmount'] = $loanArray[$i]['loanAmount'];
					}
					$selPendingInstallment 				= "select * from loan_details where loanId = ".$loanRow['loanId']." and due_date BETWEEN '".$curFromDate."' and '".$curToDate."' ";
					$selPendingInstallmentRes 			= mysql_query($selPendingInstallment);
					$pid = 0;
					if(mysql_num_rows($selPendingInstallmentRes) > 0 ) {
						while($selPendingInstallmentResRow = mysql_fetch_array($selPendingInstallmentRes))
						{
							if($selPendingInstallmentResRow['recevied'] == 0)
							{
								//$loanArray[$i]['pendingInstallment']  = "Pending";
								$pid = $pid + 1;
							}
						}
					}
					$loanArray[$i]['pendingInstallment'] = $pid;
				}
				$i++;
			}
		}
		/* echo '<pre>';
		print_r($loanArray);exit; */
		$selLoanQry = "SELECT fd_id,fd_date,fd_amount,fd_interest_rate
		                 FROM fd_amount
		                WHERE fd_party_id = ".$getDataParty." AND fd_date BETWEEN '".$curFromDate."' and '".$curToDate."'";
		$selLoanQryRes = mysql_query($selLoanQry);
		if(mysql_num_rows($selLoanQryRes) > 0 ) {
			while($loanRow = mysql_fetch_array($selLoanQryRes))
			{
				//print_r($loanRow);exit;
				$loanArray1[$k]['fd_id']           	= $loanRow['fd_id'];
				$loanArray1[$k]['partyName']        = $selectedPartyName;
				$loanArray1[$k]['fd_interest_rate'] = $loanRow['fd_interest_rate'];
				$loanArray1[$k]['fd_date']         	= $loanRow['fd_date'];
				$loanArray1[$k]['loanType']         = 'FD';
				$loanArray1[$k]['fd_amount']       	= $loanRow['fd_amount'];	
				$selPendingInstallment 				= "select * from fd_details where fd_id = ".$loanRow['fd_id']." and due_date BETWEEN '".$curFromDate."' and '".$curToDate."'";
				$selPendingInstallmentRes 			= mysql_query($selPendingInstallment);
				$reciveLoanAmount 					= '';
				$reciveLoanAmount 					= "select sum(transactionAmount) as transactionAmount from transactionnew where fd_id = ".$loanRow['fd_id']." AND creditDebit='Credit' AND transactionDate BETWEEN '".$curFromDate."' and '".$curToDate."' group by loanId";
				$rsReciveLoanAmount 				= mysql_query($reciveLoanAmount);
				
				if(mysql_num_rows($rsReciveLoanAmount) > 0 ) {
					$rsReciveLoanAmount 				 = mysql_fetch_array($rsReciveLoanAmount);
					//print_r($rsReciveLoanAmount);
					$loanArray1[$k]['pendingLoanAmount'] = $loanArray1[$k]['fd_amount'] - $rsReciveLoanAmount['transactionAmount'];
				} else {
					$loanArray1[$k]['pendingLoanAmount'] = $loanArray1[$k]['fd_amount'];
				}
				
				//$loanArray1[$k]['pendingInstallment']  = "Not Pending";
				$pifd = 0;
				if(mysql_num_rows($selPendingInstallmentRes) > 0 ) {
					while($selPendingInstallmentResRow = mysql_fetch_array($selPendingInstallmentRes))
					{
						if($selPendingInstallmentResRow['recevied'] == 0)
						{
							//$loanArray1[$k]['pendingInstallment']  = "Pending";
							$pifd  = $pifd + 1;
						}
					}
				}
				$loanArray1[$k]['pendingInstallment'] = $pifd;
				$loanArray1[$k]['pendingInterest'] = (($loanArray1[$k]['fd_amount'] * $loanArray1[$k]['fd_interest_rate']) / 100) * $loanArray1[$k]['pendingInstallment'];
				$k++;
			}
		}
		
		/*  echo '<pre>';
		print_r($loanArray1); */ 
	}
  $j++;
}
$smarty->assign("loanArray",$loanArray);
$smarty->assign("loanArray1",$loanArray1);
$smarty->assign("partyArr",$partyArr);
$smarty->assign("onePartyName",$onePartyName);
$smarty->assign("selectedParty",$selectedParty);
$smarty->assign("curDate",$curDate);
$smarty->assign("fromDate",$curFromDate);
$smarty->assign("toDate",$curToDate);
$smarty->assign("loanAmountTotal",0);
$smarty->assign("loanAmountTotal1",0);
$smarty->assign("interestReceivedTotal",0);
$smarty->assign("receivedAmountTotal",0);
$smarty->assign("pendingAmountTotal",0);
$smarty->display('pending.tpl');
?>