<?php
include "include/config.inc.php";
//print_r($_POST['ajax']);
if(isset($_POST['ajax']) && !empty($_POST['ajax']) && $_POST['ajax'] == '1'){
	ob_end_clean();
	$find	=	mysql_query("select agent_id,agent_name from agent") or print mysql_error();
		 
	$output1 = '<option value="0">- select search to -</option>';

	while($row=mysql_fetch_array($find))
	{
		$output1 .= '<option value='.$row['agent_id'].'>'.$row['agent_name'].'</option>';
	}
	echo $output1;exit;
}else if(isset($_POST['ajax']) && !empty($_POST['ajax']) && $_POST['ajax'] == '2'){
	ob_end_clean();
	$find	=	mysql_query("select partyId,partyName from party") or print mysql_error();
		 
	$output1 = '<option value="0">- select search to -</option>';

	while($row=mysql_fetch_array($find))
	{
		$output1 .= '<option value='.$row['partyId'].'>'.$row['partyName'].'</option>';
	}
	echo $output1;exit;
}else if(isset($_POST['ajax']) && !empty($_POST['ajax'])){
	$output1 = '<option value="0">- select search to -</option>';
	$output1 .= '</select>';
	echo $output1;exit;
}
$fromDate       = isset($_GET['fromDateYear']) ? $_GET['fromDateYear']."-".$_GET['fromDateMonth']."-".$_GET['fromDateDay'] : date("Y-m-d"); 
$toDate 		= isset($_GET['toDateYear']) ? $_GET['toDateYear']."-".$_GET['toDateMonth']."-".$_GET['toDateDay'] : date("Y-m-d"); 
$transactionArr = array();
$transactionQuery = '';
if( isset($_GET['search_by']) && !empty($_GET['search_by']) &&  isset($_GET['search_to']) && !empty($_GET['search_to'])){
	if($_GET['search_by'] == '1') {
		$transactionQuery = "select transactionnew.*,loan.* from transactionnew
							  left join loan on loan.loanId = transactionnew.loanId
							  WHERE transactionDate <= '".$toDate."' and transactionnew.agentId = ".$_GET['search_to']."
							  ORDER BY transactionDate, transactionId  ";
	} else if($_GET['search_by'] == '2') {
		$transactionQuery = "select transactionnew.*,loan.* from transactionnew
							  left join loan on loan.loanId = transactionnew.loanId
							  WHERE transactionDate <= '".$toDate."' and loan.partyId = ".$_GET['search_to']."
							  ORDER BY transactionDate, transactionId  ";
		
	}
} else {
	//we do Not consider "fromDate" in mysql condition, because we need to calculate "previous balance"
	$transactionQuery = "select transactionnew.*,loan.* from transactionnew
						  left join loan on loan.loanId = transactionnew.loanId
						  WHERE transactionDate <= '".$toDate."'
						  ORDER BY transactionDate, transactionId  ";
}
//echo $transactionQuery;
$transactionRes   = mysql_query($transactionQuery) or print mysql_error();

$balance 		= 0;
$credit_total 	= 0;
$debit_total  	= 0;
$i = 0;

// For previous balance : Start
//There may be many rows for < fromDate, we over write "balance", as for 1st row, we need "previous balance" only 1 time (so $i = 0 hard coded taken)
$transactionArr[0]['transactionForId'] = "";
$transactionArr[0]['creditDebit'] = "";
$transactionArr[0]['transactionDate'] = date_ymd_to_dmy($fromDate);
$transactionArr[0]['transactionAmount'] = "";
$transactionArr[0]['note'] = "Previous Balance";
$transactionArr[0]['partyId'] = 0;
$transactionArr[0]['agentId'] = 0;
$transactionArr[0]['balance'] = 0;
$i = 1;
// For previous balance : End
//print_r($transactionArr);exit;
if( mysql_num_rows($transactionRes) > 0){
	while($transactionRow = mysql_fetch_array($transactionRes))
	{
		
	  if($transactionRow['creditDebit'] == 'Credit')
		$balance += $transactionRow['transactionAmount'];
	  else
		$balance -= $transactionRow['transactionAmount'];
		
		if($transactionRow['transactionDate'] < $fromDate)
		$transactionArr[0]['balance'] = $balance;
		else if($transactionRow['transactionDate'] >= $fromDate && $transactionRow['transactionDate'] <= $toDate)
		{
		  if($transactionRow['creditDebit'] == 'Credit')
			$credit_total += $transactionRow['transactionAmount'];
		  else
			$debit_total  += $transactionRow['transactionAmount'];
		
		
		$transactionArr[$i]['transactionForId'] 	= $transactionRow['transactionForId'];
		$transactionArr[$i]['creditDebit'] 			= $transactionRow['creditDebit'];
		$transactionArr[$i]['transactionDate'] 		= date_ymd_to_dmy($transactionRow['transactionDate']);
		$transactionArr[$i]['transactionAmount'] 	= $transactionRow['transactionAmount'];
		$transactionArr[$i]['balance']           	= $balance;
		$transactionArr[$i]['note'] 				= $transactionRow['note'];
		$transactionArr[$i]['partyId'] 				= $transactionRow['partyId'];
		$transactionArr[$i]['agentId'] 				= $transactionRow['agentId'];
		if($transactionRow['loanId'] > 0)
		{
			$transactionArr[$i]['loanId'] 				= $transactionRow['loanId'];
		}
		else
		{
			$transactionArr[$i]['loanId'] 				= $transactionRow['fd_id'];
		}
		$i++;
	  }
	  else if($transactionRow['transactionDate'] > $toDate)
		break;
	}
}
$smarty->assign('SEARCH_BY',array('- select search by -','Agent','Client'));
$smarty->assign('SEARCH_TO',array('- select search to -'));

$smarty->assign('SEARCH_BY_SELECT',isset($_GET['search_by']) ? $_GET['search_by'] : '0');
$smarty->assign('SEARCH_TO_SELECT',isset($_GET['search_to']) ? $_GET['search_to'] : '0');
$smarty->assign("fromDate",$fromDate);
$smarty->assign("toDate",$toDate);
$smarty->assign("transactionArr",$transactionArr);
$smarty->assign("credit_total",$credit_total);
$smarty->assign("debit_total",$debit_total);
$smarty->assign("balance",$balance);

if(isset($_GET['format']) && $_GET['format'] == 'old')
  $smarty->display('dayBookOldFormat.tpl');
else
  $smarty->display('dayBook.tpl');
?>