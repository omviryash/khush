<?php
  include "include/config.inc.php";
  $start=$_REQUEST['startDatePass'];
  $end=$_REQUEST['endDatePass'];
  $loanStatus = $_REQUEST['loanStatus'];
  $loans = array();
  $m= 0 ;
  if($loanStatus == "current")
  {
    $selectLoans = "SELECT loandraw.loanDrawId, party.partyName, loandraw.drawCardDate, loandraw.drawInstallmentAmount, loandraw.drawInstallmentReceived
                      FROM loandraw
                      JOIN party ON party.PartyId = loandraw.partyId
                      JOIN drawinstallment ON loandraw.loanDrawId = drawinstallment.loanDrawId 
                     WHERE drawinstallment.isReceived = 'N' AND loandraw.drawCardDate  BETWEEN  '$start' AND  '$end'
                  GROUP BY loandraw.loanDrawId ORDER BY loandraw.drawCardDate";
    $selectLoansRes = mysql_query($selectLoans);
  }
  elseif($loanStatus == "old")
  {
    $selectLoans = "SELECT loandraw.loanDrawId, party.partyName, loandraw.drawCardDate, loandraw.drawInstallmentAmount, loandraw.drawInstallmentReceived
                      FROM loandraw
                      JOIN party ON party.PartyId = loandraw.partyId
                      JOIN drawinstallment ON loandraw.loanDrawId = drawinstallment.loanDrawId 
                     WHERE drawinstallment.isReceived = 'Y' AND loandraw.drawCardDate  BETWEEN  '$start' AND  '$end'
                  GROUP BY loandraw.loanDrawId ORDER BY loandraw.drawCardDate";
    $selectLoansRes = mysql_query($selectLoans);
  }
  elseif($loanStatus == "all")
  {
  	$selectLoans = "SELECT loandraw.loanDrawId, party.partyName, loandraw.drawCardDate, loandraw.drawInstallmentAmount, loandraw.drawInstallmentReceived
                      FROM loandraw
                      JOIN party ON party.PartyId = loandraw.partyId
                      JOIN drawinstallment ON loandraw.loanDrawId = drawinstallment.loanDrawId 
                     WHERE loandraw.drawCardDate  BETWEEN  '$start' AND  '$end'
                  GROUP BY loandraw.loanDrawId ORDER BY loandraw.drawCardDate";
    $selectLoansRes = mysql_query($selectLoans);
  }
  while($selectLoansRow = mysql_fetch_array($selectLoansRes))
  {
    $loans[$m]['loanDrawId']              = $selectLoansRow['loanDrawId'];
    $loans[$m]['partyName']               = $selectLoansRow['partyName'];
    $loans[$m]['drawCardDate']            = date("d-m-Y", strtotime($selectLoansRow['drawCardDate']));
    $loans[$m]['drawInstallmentReceived'] = $selectLoansRow['drawInstallmentReceived'];
    $m++;
  }
  $smarty->assign('loans',$loans);
  $smarty->display('selectDrawLoansAjax.tpl');
?>