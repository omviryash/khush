<?php
  include "include/config.inc.php";
  $today = date("Y-m-d");
  $title='';
  $subtitle='';
  $type=$_GET['type'];
  if(isset($_GET['type']) && $_GET['type']=='income')  
  {
  	$date=$_GET['date'];
	$newDate = date("Y-m-d", strtotime($date));
	$title='Income';
	$subtitle='Interest Received';
	$arrIncome = array();
	$i=0;
	$selIncome="SELECT `loanDate`,p.partyName,`loanAmount`,`interest` 
				 FROM loan l,party p WHERE l.partyId=p.partyId AND loanDate='".$newDate."'
				ORDER BY loanDate";
	$selIncomeRes=mysql_query($selIncome);
	while($selIncomeResRow=mysql_fetch_array($selIncomeRes))
	{
	   $arrIncome[$i]['loanDate'] = $selIncomeResRow['loanDate'];
	   $arrIncome[$i]['partyName']   = $selIncomeResRow['partyName'];
	   $arrIncome[$i]['loanAmount']   = $selIncomeResRow['loanAmount'];
	   $arrIncome[$i]['interest']   = $selIncomeResRow['interest'];
	   $i++;
	}
	  
	$arrInterest = array();
	$j=0;
	$selInterest="SELECT `int_date` as loanDate,p.partyName,`fd_amount` as loanAmount,`int_amount` as interest 
				 FROM fd_amount fa left join fd_interest fi on fi.fd_id=fa.fd_id left join  party p on fi.fd_party_id=p.partyId
				 WHERE fi.lidha_aapya='lidha' AND int_date='".$newDate."'
				ORDER BY int_date";
	$selInterestRes=mysql_query($selInterest);
	while($selInterestResRow=mysql_fetch_array($selInterestRes))
	{
	   $arrInterest[$j]['loanDate'] = $selInterestResRow['loanDate'];
	   $arrInterest[$j]['partyName']   = $selInterestResRow['partyName'];
	   $arrInterest[$j]['loanAmount']   = $selInterestResRow['loanAmount'];
	   $arrInterest[$j]['interest']   = $selInterestResRow['interest'];
	   $j++;
	}
  }
  if(isset($_GET['type']) && $_GET['type']=='expense')  
  {
	$title='Expense';
	$subtitle='Interest Paid';
	$date=$_GET['date'];
	$newDate = date("Y-m-d", strtotime($date));
	$title='Expense';
	$subtitle='Interest Paid';
	$arrIncome = array();
	$i=0;
	$selIncome="SELECT expence_date,expence_amount,expence_detail 
				 FROM expence WHERE expence_date='".$newDate."'
				ORDER BY expence_date";
	$selIncomeRes=mysql_query($selIncome);
	while($selIncomeResRow=mysql_fetch_array($selIncomeRes))
	{
	   $arrIncome[$i]['expence_date'] = $selIncomeResRow['expence_date'];
	   $arrIncome[$i]['expence_amount']   = $selIncomeResRow['expence_amount'];
	   $arrIncome[$i]['expence_detail']   = $selIncomeResRow['expence_detail'];
	   $i++;
	}
	  
	$arrInterest = array();
	$j=0;
	$selInterest="SELECT `int_date` as loanDate,p.partyName,fa.fd_amount as loanAmount,`int_amount` as interest 
				 FROM fd_amount fa left join fd_interest fi on fi.fd_id=fa.fd_id left join  party p on fi.fd_party_id=p.partyId
				 WHERE fi.lidha_aapya='aapya' AND int_date='".$newDate."'
				ORDER BY int_date";
	$selInterestRes=mysql_query($selInterest);
	while($selInterestResRow=mysql_fetch_array($selInterestRes))
	{
	   $arrInterest[$j]['loanDate'] = $selInterestResRow['loanDate'];
	   $arrInterest[$j]['partyName']   = $selInterestResRow['partyName'];
	   $arrInterest[$j]['loanAmount']   = $selInterestResRow['loanAmount'];
	   $arrInterest[$j]['interest']   = $selInterestResRow['interest'];
	   $j++;
	}
  }
  ///////////fetch the party Name
  
  $smarty->assign("today",$today);
  $smarty->assign("type",$type);
  $smarty->assign('arrIncome',$arrIncome);
  $smarty->assign('arrInterest',$arrInterest);
  $smarty->assign("title",$title);
  $smarty->assign("subtitle",$subtitle);
  $smarty->display('profitlossDetail.tpl');
?>