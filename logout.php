<?php
session_start();
if(!isset($_SESSION['s_activId']))
{
  header("location:checklogin.php");
}

unset($_SESSION['s_activId']);
session_destroy();
header("Location: checklogin.php");
exit();
?>