<?php
include "include/config.inc.php";
$today = date("Y-m-d", strtotime('today'));
$loans = array();
$m= 0 ;
$selectLoans = "SELECT loan.loanId, party.partyName as partyName, loan.loanDate, loan.loanAmount, loan.interest, loan.installmentAmount, loan.installmentDays, installment.isReceived
                  FROM loan
                  JOIN party ON party.PartyId = loan.partyId
                  JOIN installment ON loan.loanId = installment.loanId 
                 WHERE installment.isReceived = 'N' AND loan.loanDate =  '$today' 
                 GROUP BY loan.loanId ORDER BY loan.loanDate";
$selectLoansRes = mysql_query($selectLoans);
while($selectLoansRow = mysql_fetch_array($selectLoansRes))
{
  $loans[$m]['loanId']            = $selectLoansRow['loanId'];
  $loans[$m]['partyName']         = $selectLoansRow['partyName'];
  $loans[$m]['loanDate']          = date("d-m-Y", strtotime($selectLoansRow['loanDate']));
  $loans[$m]['loanAmount']        = $selectLoansRow['loanAmount'];
  $loans[$m]['interest']          = $selectLoansRow['interest'];
  $loans[$m]['installmentAmount'] = $selectLoansRow['installmentAmount'];
  $loans[$m]['installmentDays']   = $selectLoansRow['installmentDays'];
  // $loans[$m]['guaranter']         = $selectLoansRow['guaranter'];
  $m++;
}

$smarty->assign('loans',$loans);
$smarty->assign('today',$today);
$smarty->display('displayLoan.tpl');
?>